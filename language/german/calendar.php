<?php
// $Id: calendar.php 2 2005-11-02 18:23:29Z skalpa $
//%%%%%		Time Zone	%%%%
// german v 1.0 2007/08/27 sato-san / Rene Sato / www.XOOPS-magazine.com $
define("_CAL_SUNDAY", "Sonntag");
define("_CAL_MONDAY", "Montag");
define("_CAL_TUESDAY", "Dienstag");
define("_CAL_WEDNESDAY", "Mittwoch");
define("_CAL_THURSDAY", "Donnerstag");
define("_CAL_FRIDAY", "Freitag");
define("_CAL_SATURDAY", "Samstag");
define("_CAL_JANUARY", "Januar");
define("_CAL_FEBRUARY", "Februar");
define("_CAL_MARCH", "März");
define("_CAL_APRIL", "April");
define("_CAL_MAY", "Mai");
define("_CAL_JUNE", "Juni");
define("_CAL_JULY", "Juli");
define("_CAL_AUGUST", "August");
define("_CAL_SEPTEMBER", "September");
define("_CAL_OCTOBER", "Oktober");
define("_CAL_NOVEMBER", "November");
define("_CAL_DECEMBER", "Dezember");
define("_CAL_TGL1STD", "Ersten Tag der Woche umstellen");
define("_CAL_PREVYR", "Vorheriges Jahr (Halten für Menü)");
define("_CAL_PREVMNTH", "Nächster Monat (Halten für Menü)");
define("_CAL_GOTODAY", "Zum heutigen Tag");
define("_CAL_NXTMNTH", "Nächsten Monat (Halten für Menü)");
define("_CAL_NEXTYR", "Nächstes Jahr (Halten für Menü)");
define("_CAL_SELDATE", "Datum auswählen");
define("_CAL_DRAGMOVE", "Ziehen zum Bewegen");
define("_CAL_TODAY", "Heute");
define("_CAL_DISPM1ST", "Montag zuerst anzeigen");
define("_CAL_DISPS1ST", "Sonntag zuerst anzeigen");
?>
