<?php
// $Id: $
//%%%%%%		File Name auth.php 		%%%%%
// german v 1.0 2007/08/27 sato-san / Rene Sato / www.XOOPS-magazine.com $
define('_AUTH_MSG_AUTH_METHOD',"using %s Authentifizierungsmethode");
define('_AUTH_LDAP_EXTENSION_NOT_LOAD','PHP LDAP ist nicht geladen (Es muss die Datei php.ini auf die Konfiguration geprüft werden)');
define('_AUTH_LDAP_SERVER_NOT_FOUND',"Es kann keine Verbindung zum Server aufgebaut werden");
define('_AUTH_LDAP_USER_NOT_FOUND',"Member %s not found in the directory server (%s) in %s");
define('_AUTH_LDAP_CANT_READ_ENTRY',"Can't read entry %s");
define('_AUTH_LDAP_XOOPS_USER_NOTFOUND',"Sorry no corresponding user information has been found in the XOOPS database for connection: %s <br>" .
		"Please verify your user datas or set on the automatic provisionning");
define('_AUTH_LDAP_START_TLS_FAILED',"Es konnte keine TLS Verbindung geöffnet werden");
		
?>
