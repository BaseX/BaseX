<?php
// german v 1.0 2007/08/27 sato-san / Rene Sato / www.XOOPS-magazine.com $
define("_MAIL_MSGBODY", "Nachrichteninhalt fehlt.");
define("_MAIL_FAILOPTPL", "Fehler beim Öffnen der Vorlagendatei.");
define("_MAIL_FNAMENG", "Name des Absenders fehlt.");
define("_MAIL_FEMAILNG", "E-Mail-Adresse des Absenders fehlt.");
define("_MAIL_SENDMAILNG", "E-Mail an %s konnte nicht verschickt werden.");
define("_MAIL_MAILGOOD", "E-Mail wurde an %s verschickt.");
define("_MAIL_SENDPMNG", "Private Nachricht an %s konnte nicht verschickt werden.");
define("_MAIL_PMGOOD", "Private Nachricht an %s wurde verschickt.");

?>
