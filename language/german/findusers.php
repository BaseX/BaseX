<?php
// formselectuser.php
// german v 1.0 2007/08/27 sato-san / Rene Sato / www.XOOPS-magazine.com $
define("_MA_USER_MORE", "Mitglied finden");
define("_MA_USER_REMOVE", "Remove unselected users");


//%%%%%%	File Name findusers.php 	%%%%%
define("_MA_USER_ADD_SELECTED", "Add selected users");

define("_MA_USER_GROUP", "Gruppe");
define("_MA_USER_LEVEL", "Level");
define("_MA_USER_LEVEL_ACTIVE", "Aktiv");
define("_MA_USER_LEVEL_INACTIVE", "Inaktiv");
define("_MA_USER_LEVEL_DISABLED", "Disabled");
define("_MA_USER_RANK", "Rank");

define("_MA_USER_FINDUS","Mitglied finden");
define("_MA_USER_AVATAR","Avatar");
define("_MA_USER_REALNAME","Richtiger Name");
define("_MA_USER_REGDATE","Anmeldedatum");
define("_MA_USER_EMAIL","E-Mail");
define("_MA_USER_PREVIOUS","Previous");
define("_MA_USER_NEXT","Next");
define("_MA_USER_USERSFOUND","%s Mitglied(er) gefunden");

define("_MA_USER_ACTUS", "Aktive Mitglieder: %s");
define("_MA_USER_INACTUS", "Inaktive Mitglieder: %s");
define("_MA_USER_NOFOUND","Kein Mitglied gefunden");
define("_MA_USER_UNAME","Mitgliedsname");
define("_MA_USER_ICQ","ICQ");
define("_MA_USER_AIM","AIM");
define("_MA_USER_YIM","YIM");
define("_MA_USER_MSNM","MSN");
define("_MA_USER_LOCATION","Wohnort enthält");
define("_MA_USER_OCCUPATION","Beruf enthält");
define("_MA_USER_INTEREST","Interessen enthält");
define("_MA_USER_URLC","Homepage enthält");
define("_MA_USER_SORT","Sortieren nach");
define("_MA_USER_ORDER","Reihenfolge");
define("_MA_USER_LASTLOGIN","Zuletzt Eingeloggt");
define("_MA_USER_POSTS","Anzahl der Beiträge");
define("_MA_USER_ASC","Aufsteigende Reihenfolge");
define("_MA_USER_DESC","Absteigende Reihenfolge");
define("_MA_USER_LIMIT","Anzahl der Mitglieder pro Seite");
define("_MA_USER_RESULTS", "Suchergebnisse");
define("_MA_USER_SHOWMAILOK", "Typ des anzuzeigenden Mitglieds");
define("_MA_USER_MAILOK","Mitglieder die einer E-Mail zugestimmt haben");
define("_MA_USER_MAILNG","Mitglieder die einer E-Mail nicht zugestimmt haben");
define("_MA_USER_BOTH", "Alle");

define("_MA_USER_RANGE_LAST_LOGIN",'Zuletzt Eingeloggt vor mehr als <span style="color:#ff0000;">X</span> Tagen');
define("_MA_USER_RANGE_USER_REGDATE","Registered in past <span style='color:#ff0000;'>X</span>days");
define("_MA_USER_RANGE_POSTS","Beiträge");

define("_MA_USER_HASAVATAR", "Besitzt ein Avatar");
define("_MA_USER_MODE_SIMPLE", "Simple mode");
define("_MA_USER_MODE_ADVANCED", "Advanced mode");
define("_MA_USER_MODE_QUERY", "Query mode");
define("_MA_USER_QUERY", "Quey");

define("_MA_USER_SEARCHAGAIN", "erneut Suchen");
define("_MA_USER_NOUSERSELECTED", "Kein Mitglied ausgewählt");
define("_MA_USER_USERADDED", "Users have been added");
?>
