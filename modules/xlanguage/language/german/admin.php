<?php 
// $Id: admin.php,v 1.2 2004/09/27 20:06:50 phppp Exp $
//  ------------------------------------------------------------------------ //
//         Xlanguage: eXtensible Language Management For Xoops               //
//             Copyright (c) 2004 Xoops China Community                      //
//                    <http://www.xoops.org.cn/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: D.J.(phppp) php_pp@hotmail.com                                    //
// URL: http://www.xoops.org.cn                                              //
// ------------------------------------------------------------------------- //

define('_AM_XLANG_CONFIG', 'XLanguage Konfiguration');
define('_AM_XLANG_DELETE_CFM', 'Sind Sie sicher, die Sprache zu löschen?');
define('_AM_XLANG_DELETED', 'Die Sprache wurde gelöscht');
define('_AM_XLANG_SAVED', 'Die Sprache wurde gespeichert');
define('_AM_XLANG_EDITLANG', 'Sprache Bearbeiten');
define('_AM_XLANG_ADDLANG', 'Sprache hinzufügen');
define('_AM_XLANG_CREATED', 'Die Konfigurationsdatei wurde erstellt');
define('_AM_XLANG_CONFIGOK', 'Config-Datei verfügbar');
define('_AM_XLANG_CONFIGNOTOK', 'Config-Datei ist nicht verfügbar');
define('_AM_XLANG_ADDBASE', 'Basissprache Hinzufügen');
define('_AM_XLANG_ADDEXT', 'erweiterte Sprache Hinzufügen');
define('_AM_XLANG_CREATECONFIG', 'Konfigurationsdatei Erstellen');
define('_AM_XLANG_ABOUT', 'Über dieses Modul');
define('_AM_XLANG_LANGLIST', 'Sprachenliste');
define('_AM_XLANG_DESC', 'Desc');
define('_AM_XLANG_NAME', 'Name');
define('_AM_XLANG_CHARSET', 'charset');
define('_AM_XLANG_CODE', 'code');
define('_AM_XLANG_IMAGE', 'Bild');
define('_AM_XLANG_WEIGHT', 'Gewicht');
define('_AM_XLANG_BASE', 'Basissprache');
?>
