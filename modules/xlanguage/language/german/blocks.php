<?php
// $Id: blocks.php,v 1.1.1.3 2004/11/08 03:40:00 phppp Exp $
// Blocks
define('_MB_XLANGUAGE_SELECT', 'Klicken Sie auf die gewünschte Sprache');
define('_MB_XLANGUAGE_DISPLAY_METHOD', 'Anzeigemethode');
define('_MB_XLANGUAGE_DISPLAY_FLAGLIST', 'Bilderliste');
define('_MB_XLANGUAGE_DISPLAY_TEXTLIST', 'Textliste');
define('_MB_XLANGUAGE_DISPLAY_DROPDOWNLIST', 'Dropdown-Liste');
define('_MB_XLANGUAGE_IMAGE_SEPARATOR', 'Bild Separator');
define('_MB_XLANGUAGE_IMAGE_PERROW', 'Bilder pro Zeile');
define('_MB_XLANGUAGE_OPTIONAL', 'optional');
?>
