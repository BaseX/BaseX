<?php
// $Id: modinfo.php,v 1.2 2004/09/27 20:36:31 phppp Exp $
// Module Info

define("_MI_XLANGUAGE_NAME","Xlanguage");
define("_MI_XLANGUAGE_DESC","eXtensible multi-Language Management");

define("_MI_XLANGUAGE_BNAME","Sprachauswahl");

define("_MI_XLANGUAGE_ADMENU0","Sprachenliste");
define("_MI_XLANGUAGE_ADMENU1","Basissprache Hinzufügen");
define("_MI_XLANGUAGE_ADMENU2","erweiterte Sprache Hinzufügen");
define("_MI_XLANGUAGE_ADMENU3","Über dieses Modul");

define('_MI_XLANG_AUTHOR_INFO', "Entwicklerinformation");
define('_MI_XLANG_AUTHOR_NAME', "Entwickler");
define('_MI_XLANG_AUTHOR_WEBSITE', "Entwickler-Website");
define('_MI_XLANG_AUTHOR_EMAIL', "Entwickler E-Mail");
define('_MI_XLANG_AUTHOR_CREDITS', "Attribut");
define('_MI_XLANG_MODULE_INFO', "Modulentwicklung Informationen");
define('_MI_XLANG_MODULE_STATUS', "Entwicklungsstatus");
define('_MI_XLANG_MODULE_DEMO', "Demo Site");
define('_MI_XLANG_MODULE_SUPPORT', "Offizielle Support-Website");
define('_MI_XLANG_RELEASE', "Veröffentlichungsdatum ");
define('_MI_XLANG_AUTHOR_BUGFIXES', "Bug fix Geschichte");
define('_MI_XLANG_MODULE_XOOPSVERSION', "BaseX Version");
?>
