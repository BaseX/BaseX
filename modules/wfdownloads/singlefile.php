<?php
/**
 * $Id: singlefile.php v 1.04 06 july 2004 Liquid Exp $
 * Module: WF-Downloads
 * Version: v2.0.5a
 * Release Date: 26 july 2004
 * Author: WF-Sections
 * Licence: GNU
 */

include 'header.php';
include_once XOOPS_ROOT_PATH . '/class/xoopstree.php';

$lid = intval($_GET['lid']);
$cid = intval($_GET['cid']);
$xoopsOption['template_main'] = 'wfdownloads_singlefile.html';

$sql = "SELECT * FROM " . $xoopsDB->prefix('wfdownloads_downloads') . " WHERE lid = $lid";
$result = $xoopsDB->query($sql);
$down_arr = $xoopsDB->fetchArray($result);

if (!$down_arr) {
   redirect_header("index.php", 1, _MD_WFD_NODOWNLOAD);
   exit(); 
}

include XOOPS_ROOT_PATH . '/header.php';

/**
 * Begin Main page Heading etc
 */
$down['imageheader'] = wfd_imageheader();
$down['id'] = intval($down_arr['lid']);
$down['cid'] = intval($down_arr['cid']);
/**
 * Breadcrumb
 */
$mytree = new XoopsTree($xoopsDB->prefix('wfdownloads_cat'), "cid", "pid");
$pathstring = "<a href='index.php'>" . _MD_WFD_MAIN . "</a>&nbsp;:&nbsp;";
$pathstring .= $mytree->getNicePathFromId($cid, "title", "viewcat.php?op=");
$down['path'] = $pathstring;

include_once XOOPS_ROOT_PATH . '/modules/' . $xoopsModule->dirname() . '/include/downloadinfo.php';

$xoopsTpl->assign('show_screenshot', false);
if (isset($xoopsModuleConfig['screenshot']) && $xoopsModuleConfig['screenshot'] == 1)
{
    $xoopsTpl->assign('shots_dir', $xoopsModuleConfig['screenshots']);
    $xoopsTpl->assign('shotwidth', $xoopsModuleConfig['shotwidth']);
    $xoopsTpl->assign('shotheight', $xoopsModuleConfig['shotheight']);
    $xoopsTpl->assign('show_screenshot', true);
} 
/**
 * Show other author downloads
 */
$groups = (is_object($xoopsUser)) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
$gperm_handler = &xoops_gethandler('groupperm');

$sql = "SELECT lid, cid, title, published FROM " . $xoopsDB->prefix('wfdownloads_downloads') . " 
	WHERE submitter = " . $down_arr['submitter'] . " 
	AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") 
	AND offline = 0 ORDER by published DESC";
$result = $xoopsDB->query($sql, 20, 0);

while ($arr = $xoopsDB->fetchArray($result))
{
    if (!$gperm_handler->checkRight('WFDownFilePerm', $arr['lid'], $groups, $xoopsModule->getVar('mid')) || $arr['lid'] == $lid)
        continue;
 
    $downuid['title'] = $arr['title'];
    $downuid['lid'] = $arr['lid'];
    $downuid['cid'] = $arr['cid'];
    $downuid['published'] = formatTimestamp($arr['published'], $xoopsModuleConfig['dateformat']);;
    $xoopsTpl->append('down_uid', $downuid);
} 
/**
 * User reviews
 */
$sql_review = "SELECT * FROM " . $xoopsDB->prefix('wfdownloads_reviews') . " 
	WHERE lid = " . $down_arr['lid'] . " AND submit = 1";
$result_review = $xoopsDB->query($sql_review);
$review_amount = $xoopsDB->getRowsNum($result_review);
if ($review_amount > 0)
{
    $user_reviews = "op=list&amp;cid=" . $down_arr['cid'] . "&amp;lid=" . $down_arr['lid'] . "\">" . _MD_WFD_USERREVIEWS;
} 
else
{
    $user_reviews = "cid=" . $down_arr['cid'] . "&amp;lid=" . $down_arr['lid'] . "\">" . _MD_WFD_NOUSERREVIEWS;
} 
$xoopsTpl->assign('lang_user_reviews', $xoopsConfig['sitename'] . " " . _MD_WFD_USERREVIEWSTITLE);
$xoopsTpl->assign('lang_UserReviews', sprintf($user_reviews, $down_arr['title']));

if (isset($xoopsModuleConfig['copyright']) && $xoopsModuleConfig['copyright'] == 1)
{
    $xoopsTpl->assign('lang_copyright', "" . $down['title'] . " � " . _MD_WFD_COPYRIGHT . " " . date("Y") . " " . XOOPS_URL);
} 
$xoopsTpl->assign('down', $down);

include XOOPS_ROOT_PATH . '/include/comment_view.php';
include XOOPS_ROOT_PATH . '/footer.php';

?>
