<?php 
/* $Id: ratefile.php,v 2.3 11 july 2004 Liquid Exp $
 * Module: WF-Downloads
 * Version: v2.0.5a
 * Release Date: 26 july 2004
 * Author: WF-Sections
 * Licence: GNU
 */
 
include 'header.php';

global $myts;

if (!empty($_POST['submit']))
{
    if (empty($xoopsUser))
    {
        $ratinguser = 0;
    } 
    else
    {
        $ratinguser = $xoopsUser -> getVar('uid');
    } 
    // Make sure only 1 anonymous from an IP in a single day.
    $anonwaitdays = 1;
    $ip = getenv("REMOTE_ADDR");
    $lid = intval($_POST['lid']);
    $cid = intval($_POST['cid']);
    $rating = intval($_POST['rating']); 
    // Check if Rating is Null
    if ($rating == "--")
    {
        redirect_header("ratefile.php?cid=" . $cid . "&amp;lid=" . $lid . "", 4, _MD_WFD_NORATING);
        exit();
    } 
    // Check if Download POSTER is voting (UNLESS Anonymous users allowed to post)
    if ($ratinguser != 0)
    {
        $result = $xoopsDB -> query("SELECT submitter FROM " . $xoopsDB -> prefix('wfdownloads_downloads') . " WHERE lid=$lid");
        while (list($ratinguserDB) = $xoopsDB -> fetchRow($result))
        {
            if ($ratinguserDB == $ratinguser)
            {
                redirect_header("index.php", 4, _MD_WFD_CANTVOTEOWN);
                exit();
            } 
        } 
        // Check if REG user is trying to vote twice.
        $result = $xoopsDB -> query("SELECT ratinguser FROM " . $xoopsDB -> prefix('wfdownloads_votedata') . " WHERE lid=$lid");
        while (list($ratinguserDB) = $xoopsDB -> fetchRow($result))
        {
            if ($ratinguserDB == $ratinguser)
            {
                redirect_header('index.php', 4, _MD_WFD_VOTEONCE);
                exit();
            } 
        } 
    } 
    else
    { 
        // Check if ANONYMOUS user is trying to vote more than once per day.
        $yesterday = (time() - (86400 * $anonwaitdays));
        $result = $xoopsDB -> query("SELECT COUNT(*) FROM " . $xoopsDB -> prefix('wfdownloads_votedata') . " WHERE lid=$lid AND ratinguser=0 AND ratinghostname = '$ip'  AND ratingtimestamp > $yesterday");
        list($anonvotecount) = $xoopsDB -> fetchRow($result);
        if ($anonvotecount >= 1)
        {
            redirect_header("index.php", 4, _MD_WFD_VOTEONCE);
            exit();
        } 
    } 
    // All is well.  Add to Line Item Rate to DB.
    $newid = $xoopsDB -> genId($xoopsDB -> prefix('wfdownloads_votedata') . "_ratingid_seq");
    $datetime = time();
    $sql = sprintf("INSERT INTO %s (ratingid, lid, ratinguser, rating, ratinghostname, ratingtimestamp) VALUES (%u, %u, %u, %u, '%s', %u)", $xoopsDB -> prefix('wfdownloads_votedata'), $newid, $lid, $ratinguser, $rating, $ip, $datetime);
    $xoopsDB -> query($sql); 
    // All is well.  Calculate Score & Add to Summary (for quick retrieval & sorting) to DB.
    wfd_updaterating($lid);
    $ratemessage = _MD_WFD_VOTEAPPRE . "<br />" . sprintf(_MD_WFD_THANKYOU, $xoopsConfig['sitename']);
    redirect_header('index.php', 4, $ratemessage);
    exit();
} 
else
{
    $xoopsOption['template_main'] = 'wfdownloads_ratefile.html';
    include XOOPS_ROOT_PATH . '/header.php';
    $lid = intval($_GET['lid']);
    $cid = intval($_GET['cid']);
    $imageheader = wfd_imageheader();

    $result = $xoopsDB -> query("SELECT title FROM " . $xoopsDB -> prefix('wfdownloads_downloads') . " WHERE lid=$lid");
    list($title) = $xoopsDB -> fetchRow($result);
    $xoopsTpl -> assign('file', array('id' => $lid, 'cid' => $cid, 'title' => $myts -> htmlSpecialChars($title), 'imageheader' => $imageheader));
    include XOOPS_ROOT_PATH . '/footer.php';
} 
include 'footer.php';

?>