<?php
/**
 * $Id: index.php v 1.0.2 03 july 2004 Liquid Exp $
 * Module: WF-Downloads
 * Version: v2.0.4
 * Release Date: 11 july 2004
 * Author: WF-Sections
 * Licence: GNU
 */

$adminmenu[1]['title'] = _MI_WFD_BINDEX;
$adminmenu[1]['link'] = "admin/index.php";
$adminmenu[2]['title'] = _MI_WFD_INDEXPAGE;
$adminmenu[2]['link'] = "admin/indexpage.php";
$adminmenu[3]['title'] = _MI_WFD_MCATEGORY;
$adminmenu[3]['link'] = "admin/category.php";
$adminmenu[4]['title'] = _MI_WFD_MDOWNLOADS;
$adminmenu[4]['link'] = "admin/index.php?op=Download";
$adminmenu[5]['title'] = _MI_WFD_MUPLOADS;
$adminmenu[5]['link'] = "admin/upload.php";
$adminmenu[6]['title'] = _MI_WFD_MMIMETYPES;
$adminmenu[6]['link'] = "admin/mimetypes.php";
$adminmenu[7]['title'] = _MI_WFD_MVOTEDATA;
$adminmenu[7]['link'] = "admin/votedata.php";
$adminmenu[8]['title'] = _MI_WFD_PERMISSIONS;
$adminmenu[8]['link'] = "admin/permissions.php"; 
$adminmenu[9]['title'] = _MI_WFD_BLOCKADMIN;
$adminmenu[9]['link'] = "admin/myblocksadmin.php"; 

?>