<?php
include 'admin_header.php';
/**
 * 
 * @version $Id$
 * @copyright 2003
 */
global $_GET, $_POST;

$g_id = 1;

xoops_cp_header();

$member_handler = & xoops_gethandler('member');
$thisgroup = & $member_handler -> getGroup($g_id);
$name_value = $thisgroup -> getVar("name", "E");
$desc_value = $thisgroup -> getVar("description", "E");
$moduleperm_handler = & xoops_gethandler('groupperm');

wfd_adminmenu(_AM_WFD_EDITBANNED);

$usercount = $member_handler -> getUserCount(new Criteria('level', 0, '>'));
$member_handler = & xoops_gethandler('member');
$membercount = $member_handler -> getUserCountByGroup($g_id);

$members = & $member_handler -> getUsersByGroup($g_id, true);
$mlist = array();
$mcount = count($members);
for ($i = 0; $i < $mcount; $i++)
{
    $mlist[$members[$i] -> getVar('uid')] = $members[$i] -> getVar('uname');
} 
$criteria = new Criteria('level', 0, '>');
$criteria -> setSort('uname');
$userslist = & $member_handler -> getUserList($criteria);
$users = & array_diff($userslist, $mlist);

echo '<table class="outer">
		<tr><th align="center">' . _AM_WFD_NONBANNED . '<br />';

echo '</th><th></th><th align="center">' . _AM_WFD_BANNED . '<br />';
echo '</th></tr>
		<tr><td class="even">
		<form action="admin.php" method="post">
		<select name="uids[]" size="10" multiple="multiple">' . "\n";
foreach ($mlist as $m_id => $m_name)
{
    echo '<option value="' . $m_id . '">' . $m_name . '</option>' . "\n";
} 

echo '</select>';
echo "</td><td align='center' class='odd'>
		<input type='hidden' name='op' value='addUser' />
		<input type='hidden' name='fct' value='groups' />
		<input type='hidden' name='groupid' value='" . $thisgroup -> getVar("groupid") . "' />
		<input type='submit' name='submit' value='" . _AM_WFD_BADD . "' />
		</form><br />
		<form action='admin.php' method='post' />
		<input type='hidden' name='op' value='delUser' />
		<input type='hidden' name='fct' value='groups' />
		<input type='hidden' name='groupid' value='" . $thisgroup -> getVar("groupid") . "' />
		<input type='submit' name='submit' value='" . _AM_WFD_BDELETE . "' />
		</td>
		<td class='even'>";
echo "<select name='uids[]' size='10' multiple='multiple'>";
foreach ($users as $u_id => $u_name)
{
    echo '<option value="' . $u_id . '">' . $u_name . '</option>' . "\n";
} 
echo "</select>";
echo '</td></tr>
		</form>
		</table>';
xoops_cp_footer();

?>