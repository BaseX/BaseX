<?php
if (!defined('IS_UPDATE_FILE')) {
	echo "Cannot access this file directly!";
	exit();
}
/**
 * $Id: wfdownloads.php v 1.02 06 july 2004 Liquid Exp $
 * Module: WF-Downloads
 * Version: v2.0.5
 * Release Date: 25 july 2004
 * Author: WF-Sections
 * Licence: GNU
 */
$error = array();
$output = array();

$result = $xoopsDB -> queryF("ALTER TABLE " . $xoopsDB -> prefix("wfdownloads_downloads") . " CHANGE publisher publisher text NOT NULL");
if (!$result)
{
    $error[] = "<b>Error:</b> Could <span style='color:#ff0000;font-weight:bold'>not change </span> <b>publisher</b> field in database!";
} 
else
{
    $output[] = "<b>Success:</b> <b>publisher</b> <span style='color:#FF0000;font-weight:bold'>modified</span> Successfully";
} 
 
echo "<p>...Updating</p>\n";

if (count($error))
{
    foreach($error as $err)
    {
        echo $err . "<br>";
    } 
} 
if (count($output))
{
    echo "<p><span style='color:#0000FF;font-weight:bold'>There where updates made to your database. <br />Any questions? Please contact the support team at the <br><h4><a href='http://wfsections.xoops2.com'>WF-Section website</a></h4></span></p>\n";
    foreach($output as $nonerr)
    {
        echo $nonerr . "<br>";
    } 
} 
echo "<p><span><a href=''>Finish updating Module</a></span></p>\n";

?>
