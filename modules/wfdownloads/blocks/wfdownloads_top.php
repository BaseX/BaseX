<?php 
/**
 * $Id: wfdownloads.php v 1.03 05 july 2004 Catwolf Exp $
 * Module: WF-Downloads
 * Version: v2.0.5a
 * Release Date: 26 july 2004
 * Author: WF-Sections
 * Licence: GNU
 */

/**
 * Function: b_mydownloads_top_show
 * Input   : $options[0] = date for the most recent downloads
 *                     hits for the most popular downloads
 *            $block['content'] = The optional above content
 *            $options[1]   = How many downloads are displayes
 * Output  : Returns the most recent or most popular downloads
 */
include_once XOOPS_ROOT_PATH . '/class/xoopsform/grouppermform.php';

function b_wfdownloads_top_show($options)
{
    global $xoopsDB, $xoopsModule, $xoopsUser;

    $block = array();
    $myts = &MyTextSanitizer::getInstance();

    $modhandler = &xoops_gethandler('module');
    $xoopsModule = &$modhandler->getByDirname("wfdownloads");
    $config_handler = &xoops_gethandler('config');
    $xoopsModuleConfig = &$config_handler->getConfigsByCat(0, $xoopsModule->getVar('mid'));

    $groups = (is_object($xoopsUser)) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
    $gperm_handler = &xoops_gethandler('groupperm'); 

    $result = $xoopsDB->query("SELECT lid, cid, title, date, hits FROM " . $xoopsDB->prefix('wfdownloads_downloads') . " WHERE status > 0 AND offline = 0 ORDER BY " . $options[0] . " DESC", $options[1], 0);
    while($myrow = $xoopsDB->fetchArray($result))
    {
        if ($gperm_handler->checkRight('WFDownFilePerm', $myrow['lid'], $groups, $xoopsModule->getVar('mid')))
        {
            $download = array();
            $title = $myts->htmlSpecialChars($myrow["title"]);
            if (!XOOPS_USE_MULTIBYTES)
            {
                if (strlen($myrow['title']) >= $options[2])
                {
                    $title = $myts->htmlSpecialChars(substr($myrow['title'], 0, ($options[2] -1))) . "...";
                }
            }
            $download['id'] = $myrow['lid'];
            $download['cid'] = $myrow['cid'];
            $download['title'] = $title;
            if ($options[0] == "date")
            {
                $download['date'] = formatTimestamp($myrow['date'], $xoopsModuleConfig['dateformat']);
            }elseif ($options[0] == "hits")
            {
                $download['hits'] = $myrow['hits'];
            }
            $download['dirname'] = $xoopsModule->dirname();
			$block['downloads'][] = $download;
        }
    }
    return $block;
}

function b_wfdownloads_top_edit($options)
{
    $form = "" . _MB_WFD_DISP . "&nbsp;";
    $form .= "<input type='hidden' name='options[]' value='";
    if ($options[0] == "date")
    {
        $form .= "date'";
    }
    else
    {
        $form .= "hits'";
    }
    $form .= " />";
    $form .= "<input type='text' name='options[]' value='" . $options[1] . "' />&nbsp;" . _MB_WFD_FILES . "";
    $form .= "&nbsp;<br />" . _MB_WFD_CHARS . "&nbsp;<input type='text' name='options[]' value='" . $options[2] . "' />&nbsp;" . _MB_WFD_LENGTH . "";
    return $form;
}

?>
