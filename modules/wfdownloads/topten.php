<?php
/**
 * $Id: topten.php v 1.0.3 06 july 2004 Liquid Exp $
 * Module: WF-Downloads
 * Version: v2.0.5a
 * Release Date: 26 july 2004
 * Author: WF-Sections
 * Licence: GNU
 */

include 'header.php';
include_once XOOPS_ROOT_PATH . '/class/xoopstree.php';

global $xoopsDB, $xoopsUser;

$mytree = new XoopsTree($xoopsDB -> prefix('wfdownloads_cat'), "cid", "pid");
$xoopsOption['template_main'] = 'wfdownloads_topten.html';

$groups = (is_object($xoopsUser)) ? $xoopsUser -> getGroups() : XOOPS_GROUP_ANONYMOUS;
$module_id = $xoopsModule -> getVar('mid');
$gperm_handler = & xoops_gethandler('groupperm');

include XOOPS_ROOT_PATH . '/header.php';

$action_array = array('hit' => 0, 'rate' => 1);
$list_array = array('hits', 'rating');
$lang_array = array(_MD_WFD_HITS, _MD_WFD_RATING);

$sort = (isset($_GET['list']) && in_array($_GET['list'], $action_array)) ? $_GET['list'] : 'rate';
$this = $action_array[$sort];
$sortDB = $list_array[$this];

$catarray['imageheader'] = wfd_imageheader();
$catarray['letters'] = wfd_letters();
$catarray['toolbar'] = wfd_toolbar();
$xoopsTpl -> assign('catarray', $catarray);

$arr = array();
$result = $xoopsDB -> query("SELECT cid, title FROM " . $xoopsDB -> prefix('wfdownloads_cat') . " WHERE pid=0");

$e = 0;
$rankings = array();
while (list($cid, $ctitle) = $xoopsDB -> fetchRow($result))
{
    if ($gperm_handler -> checkRight('WFDownCatPerm', $cid , $groups, $module_id))
    {
        $query = "SELECT lid, cid, title, hits, rating, votes, platform FROM " . $xoopsDB -> prefix('wfdownloads_downloads') . " WHERE published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND offline = 0 AND (cid=$cid";
        $arr = $mytree -> getAllChildId($cid);
        for($i = 0;$i < count($arr);$i++)
        {
            $query .= " or cid=" . $arr[$i] . "";
        } 
        $query .= ") order by " . $sortDB . " DESC";
        $result2 = $xoopsDB -> query($query, 10, 0);
        $filecount = $xoopsDB -> getRowsNum($result2);

        if ($filecount > 0)
        {
            $rankings[$e]['title'] = $myts -> htmlSpecialChars($ctitle);
            $rank = 1;

            while (list($did, $dcid, $dtitle, $hits, $rating, $votes) = $xoopsDB -> fetchRow($result2))
            {
                if ($gperm_handler -> checkRight('WFDownFilePerm', $did, $groups, $xoopsModule -> getVar('mid')))
                {
                    $catpath = $mytree -> getPathFromId($dcid, "title");
                    $catpath = basename($catpath);

                    $dtitle = $myts -> htmlSpecialChars($dtitle);
                    //if ($catpath != $ctitle)
                    //{
                    //    $dtitle = $myts -> htmlSpecialChars($ctitle); //. $ctitle;
                    //} 

                    $rankings[$e]['file'][] = array('id' => $did, 'cid' => $dcid, 'rank' => $rank, 'title' => $dtitle, 'category' => $catpath, 'hits' => $hits, 'rating' => number_format($rating, 2), 'votes' => $votes);
                    $rank++;
                } 
            } 
            $e++;
        } 
    } 
} 

$xoopsTpl -> assign('lang_sortby' , $lang_array[$this]);



$xoopsTpl -> assign('rankings', $rankings);
include XOOPS_ROOT_PATH . '/footer.php';

include 'footer.php';

?>
