<?php
/**
 * $Id: visit.php v 1.02 06 july 2004 Liquid Exp $
 * Module: WF-Downloads
 * Version: v2.0.5a
 * Release Date: 26 july 2004
 * Author: WF-Sections
 * Licence: GNU
 */

include 'header.php';

global $xoopsUser, $xoopsModuleConfig, $myts;

$agreed = (isset($_GET['agree'])) ? $_GET['agree'] : 0;

$lid = intval($_GET['lid']);
$cid = intval($_GET['cid']);

function reportBroken($lid)
{
    global $xoopsModule;
    echo "
		<h4>" . _MD_WFD_BROKENFILE . "</h4>\n
		<div>" . _MD_WFD_PLEASEREPORT . "\n
		<a href='" . XOOPS_URL . "/modules/wfdownloads/brokenfile.php?lid=$lid'>" . _MD_WFD_CLICKHERE . "</a>\n
		</div>\n";
} 

if ($agreed == 0)
{
    if ($xoopsModuleConfig['check_host'])
    {
        $goodhost = 0;
        $referer = parse_url(xoops_getenv('HTTP_REFERER'));
        $referer_host = $referer['host'];
        foreach ($xoopsModuleConfig['referers'] as $ref)
        {
            if (!empty($ref) && preg_match("/" . $ref . "/i", $referer_host))
            {
                $goodhost = "1";
                break;
            } 
        } 
        if (!$goodhost)
        {
            redirect_header(XOOPS_URL . "/modules/wfdownloads/singlefile.php?cid=$cid&amp;lid=$lid", 20, _MD_WFD_NOPERMISETOLINK);
            exit();
        } 
    } 
} 

if ($xoopsModuleConfig['showDowndisclaimer'] && $agreed == 0)
{
    include XOOPS_ROOT_PATH . '/header.php';
    echo "
		<div align='center'>" . wfd_imageheader() . "</div>\n
		<h4>" . _MD_WFD_DISCLAIMERAGREEMENT . "</h4>\n
		<div>" . $myts -> displayTarea($xoopsModuleConfig['downdisclaimer'], 0, 1, 1, 1, 1) . "</div><br />\n
		<form action='visit.php' method='post'>\n
		<div align='center'><b>" . _MD_WFD_DOYOUAGREE . "</b><br /><br />\n
		<input type='button' onclick='location=\"visit.php?agree=1&amp;lid=$lid&amp;cid=$cid\"' class='formButton' value='" . _MD_WFD_AGREE . "' alt='" . _MD_WFD_AGREE . "' />\n
		&nbsp;\n
		<input type='button' onclick='location=\"index.php\"' class='formButton' value='" . _CANCEL . "' alt='" . _CANCEL . "' />\n
		<input type='hidden' name='lid' value='1' />\n
		<input type='hidden' name='cid' value='1' />\n
		</div></form>\n";
    include XOOPS_ROOT_PATH . '/footer.php';
    exit();
} 
else
{
    $isadmin = (!empty($xoopsUser) && $xoopsUser -> isAdmin($xoopsModule -> mid())) ? true : false;
    if ($isadmin == false)
    {
        $sql = sprintf("UPDATE " . $xoopsDB -> prefix('wfdownloads_downloads') . " SET hits = hits+1 WHERE lid =$lid");
        $xoopsDB -> queryF($sql);
    } 
    $result = $xoopsDB -> query("SELECT url FROM " . $xoopsDB -> prefix('wfdownloads_downloads') . " WHERE lid=$lid");
    list($url) = $xoopsDB -> fetchRow($result);

    include XOOPS_ROOT_PATH . '/header.php';
    echo "<br /><div align='center'>" . wfd_imageheader() . "</div>";
    $url = $myts -> htmlSpecialChars(preg_replace('/javascript:/si' , 'java script:', $url), ENT_QUOTES);

    if (!empty($url))
    {
        if (!headers_sent())
        {
        	echo "
			<h4><img src='" . XOOPS_URL . "/modules/wfdownloads/images/icon/downloads.gif' align='middle' alt='' /> " . _MD_WFD_DOWNINPROGRESS . "</h4>\n
			<div>" . _MD_WFD_DOWNSTARTINSEC . "</div><br />\n
			<div>" . _MD_WFD_DOWNNOTSTART . "\n
			<a href='$url' target='_blank'>" . _MD_WFD_CLICKHERE . "</a>.\n
			</div>\n";

            if (!empty($url))
            {
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false); 
                // HTTP/1.0
                header("Pragma: no-cache"); 
                // Date in the past
                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
                // always modified
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Refresh: 3; url=$url");
            } 
            else
            {
                include XOOPS_ROOT_PATH . '/header.php';
                echo "<br /><div align='center'>" . wfd_imageheader() . "</div>";
                reportBroken($lid);
            } 
        } else {
			die("Headers already sent"); 
		}
    } 
    else
    {
        reportBroken($lid);
    } 
    include XOOPS_ROOT_PATH . '/footer.php';
} 

?>
