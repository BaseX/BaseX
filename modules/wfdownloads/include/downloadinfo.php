<?php
/**
 * $Id: downloadinfo.php v 1.01 02 july 2004 Liquid Exp $
 * Module: WF-Downloads
 * Version: v2.0.5a
 * Release Date: 26 july 2004
 * Author: WF-Sections
 * Licence: GNU
 */

$down['id'] = intval($down_arr['lid']);
$down['cid'] = intval($down_arr['cid']);

$path = $mytree->getPathFromId($down_arr['cid'], "title");
$path = substr($path, 1);
$path = basename($path);
$path = str_replace("/", "", $path);
$down['category'] = $path;

$rating = round(number_format($down_arr['rating'], 0) / 2);
$rateimg = "rate$rating.gif";
$down['rateimg'] = $rateimg;
$down['votes'] = ($down_arr['votes'] == 1) ? _MD_WFD_ONEVOTE : sprintf(_MD_WFD_NUMVOTES, $down_arr['votes']);
$down['hits'] = intval($down_arr['hits']);

$xoopsTpl->assign('lang_dltimes', sprintf(_MD_WFD_DLTIMES, $down['hits']));

$down['title'] = $down_arr['title'];
$down['url'] = $down_arr['url'];

if (isset($down_arr['screenshot']))
{
    $down['screenshot_full'] = $myts->htmlSpecialChars($down_arr['screenshot']);
    if (!empty($down_arr['screenshot']) && file_exists(XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['screenshots'] . "/" . xoops_trim($down_arr['screenshot'])))
    {
        if (isset($xoopsModuleConfig['usethumbs']) && $xoopsModuleConfig['usethumbs'] == 1)
        {
            $down['screenshot_thumb'] = down_createthumb($down['screenshot_full'], $xoopsModuleConfig['screenshots'], "thumbs", $xoopsModuleConfig['shotwidth'], $xoopsModuleConfig['shotheight'], 
				$xoopsModuleConfig['imagequality'], $xoopsModuleConfig['updatethumbs'], $xoopsModuleConfig['keepaspect']);
        } else {
            $down['screenshot_thumb'] = XOOPS_URL . "/" . $xoopsModuleConfig['screenshots'] . "/" . xoops_trim($down_arr['screenshot']);
		}
    } 
} 

$down['homepage'] = (!$down_arr['homepage'] || $down_arr['homepage'] == "http://") ? '' : $myts->htmlSpecialChars(trim($down_arr['homepage']));
if ($down['homepage'] && !empty($down['homepage']))
{
    $down['homepagetitle'] = (empty($down_arr['homepagetitle'])) ? trim($down['homepage']) : $myts->htmlSpecialChars(trim($down_arr['homepagetitle']));
    $down['homepage'] = "<a href='" . $down['homepage'] . "' target='_blank'>" . $down['homepagetitle'] . "</a>";
} 
else
{
    $down['homepage'] = _MD_WFD_NOTSPECIFIED;
} 

$down['mirror'] = ($down_arr['mirror'] == "http://") ? '' : $myts->htmlSpecialChars(trim($down_arr['mirror']));
$down['mirror'] = ($down['mirror']) ? "<a href='" . $down['mirror'] . "' target='_blank'>" . _MD_WFD_MIRRORSITE . "</a>" : _MD_WFD_NOTSPECIFIED;
$down['comments'] = $down_arr['comments'];
$down['version'] = $down_arr['version'];
$down['downtime'] = wfd_GetDownloadTime(intval($down_arr['size']), 1, 1, 1, 1, 0);
$down['downtime'] = str_replace("|", "<br />", $down['downtime']);
$down['size'] = wfd_PrettySize(intval($down_arr['size']));

$time = ($down_arr['updated'] != 0) ? $down_arr['updated'] : $down_arr['published'];
$down['updated'] = formatTimestamp($time, $xoopsModuleConfig['dateformat']);
$is_updated = ($down_arr['updated'] != 0) ? _MD_WFD_UPDATEDON : _MD_WFD_SUBMITDATE;
$xoopsTpl->assign('lang_subdate' , $is_updated);

$down['description'] = $myts->displayTarea($down_arr['description'], 0); //no html
$down['price'] = ($down_arr['price'] != 0) ? intval($down_arr['price']) : _MD_WFD_PRICEFREE;
$down['limitations'] = (empty($down_arr['limitations'])) ? _MD_WFD_NOTSPECIFIED : $myts->htmlSpecialChars(trim($xoopsModuleConfig['limitations'][$down_arr['limitations']]));
$down['license'] = (empty($down_arr['license'])) ? _MD_WFD_NOTSPECIFIED : $myts->htmlSpecialChars(trim($xoopsModuleConfig['license'][$down_arr['license']]));
$down['submitter'] = xoops_getLinkedUnameFromId(intval($down_arr['submitter']));
$down['publisher'] = (isset($down_arr['publisher']) && !empty($down_arr['publisher'])) ? $myts->htmlSpecialChars($down_arr['publisher']) : _MD_WFD_NOTSPECIFIED;
$down['platform'] = $myts->htmlSpecialChars($xoopsModuleConfig['platform'][$down_arr['platform']]);
$down['history'] = $myts->displayTarea($down_arr['dhistory'],1);
$down['features'] = '';
if ($down_arr['features'])
{
    $downfeatures = explode('|', trim($down_arr['features']));
    foreach ($downfeatures as $bi)
    {
        $down['features'][] = $bi;
    } 
} 

$down['requirements'] = '';
if ($down_arr['requirements'])
{
    $downrequirements = explode('|', trim($down_arr['requirements']));
    foreach ($downrequirements as $bi)
    {
        $down['requirements'][] = $bi;
    } 
} 
$down['mail_subject'] = rawurlencode(sprintf(_MD_WFD_INTFILEFOUND, $xoopsConfig['sitename']));
$down['mail_body'] = rawurlencode(sprintf(_MD_WFD_INTFILEFOUND, $xoopsConfig['sitename']) . ':  ' . XOOPS_URL . '/modules/wfdownloads/singlefile.php?cid=' . $down_arr['cid'] . '&amp;lid=' . $down_arr['lid']); 

$down['isadmin'] = (!empty($xoopsUser) && $xoopsUser->isAdmin($xoopsModule->mid())) ? true : false;

$down['adminlink'] = '';
if ($down['isadmin'] == true)
{
    $down['adminlink'] = '[ <a href="' . XOOPS_URL . '/modules/wfdownloads/admin/index.php?op=Download&amp;lid=' . $down_arr['lid'] . '">' . _MD_WFD_EDIT . '</a> | ';
    $down['adminlink'] .= '<a href="' . XOOPS_URL . '/modules/wfdownloads/admin/index.php?op=delDownload&amp;lid=' . $down_arr['lid'] . '">' . _MD_WFD_DELETE . '</a> ]';
} 
$votestring = ($down_arr['votes'] == 1) ? _MD_WFD_ONEVOTE : sprintf(_MD_WFD_NUMVOTES, $down_arr['votes']);
$is_updated = ($down_arr['updated'] > 0) ? _MD_WFD_UPDATEDON : _MD_WFD_SUBMITDATE;
$xoopsTpl->assign('lang_subdate' , $is_updated);
if (is_object($xoopsUser) && $down['isadmin'] != true)
{
    $down['useradminlink'] = ($xoopsUser->getvar('uid') == $down_arr['submitter']) ? true : false;
} 

$sql2 = "SELECT rated FROM " . $xoopsDB->prefix('wfdownloads_reviews') . " WHERE lid = " . $down_arr['lid'] . " AND submit = 1";
$results = $xoopsDB->query($sql2);
$numrows = $xoopsDB->getRowsNum($results);

$down['reviews_num'] = ($numrows) ? $numrows : 0;

$finalrating = 0;
$totalrating = 0;

while ($review_text = $xoopsDB->fetchArray($results))
{
    $totalrating += $review_text['rated'];
} 

if ($down['reviews_num'] > 0)
{
    $finalrating = $totalrating / $down['reviews_num'];
    $finalrating = round(number_format($finalrating, 0) / 2);
} 
$down['review_rateimg'] = "rate$finalrating.gif";;

$modhandler = &xoops_gethandler('module');
$xoopsforumModule = &$modhandler->getByDirname("newbb");
if (is_object($xoopsforumModule) && $xoopsforumModule->getVar('isactive'))
{
    $down['forumid'] = ($down_arr['forumid'] > 0) ? $down_arr['forumid'] : 0;
} 

$down['icons'] = wfd_displayicons($down_arr['published'], $down_arr['status'], $down_arr['hits']);  
$xoopsTpl->append('file', $down);

?>