<?php
// german v 1.0 2007/08/27 sato-san / Rene Sato / www.XOOPS-magazine.com $
//%%%%%%	File Name  modulesadmin.php 	%%%%%
define('_MD_AM_MODADMIN','Moduladministration');
define('_MD_AM_MODULE','Modul');
define('_MD_AM_VERSION','Version');
define('_MD_AM_LASTUP','Letzte Aktualisierung');
define('_MD_AM_DEACTIVATED','Deaktiviert');
define('_MD_AM_ACTION','Aktion');
define('_MD_AM_DEACTIVATE','Deaktivieren');
define('_MD_AM_ACTIVATE','Aktivieren');
define('_MD_AM_UPDATE','Aktualisieren');
define('_MD_AM_DUPEN','Doppelter Eintrag in der Modultabelle!');
define('_MD_AM_DEACTED','Das gewählte Modul wurde deaktiviert. Sie können das Modul jetzt gefahrlos deinstallieren');
define('_MD_AM_ACTED','Das gewählte Modul wurde aktiviert!');
define('_MD_AM_UPDTED','Das gewählte Modul wurde aktualisiert!');
define('_MD_AM_SYSNO','Das Systemmodul kann nicht deaktiviert werden.');
define('_MD_AM_STRTNO','Dieses Modul wird auf der Startseite sichtbar sein. Bitte stellen sie es Ihren Wünschen entsprechend ein.');

// added in RC2
define('_MD_AM_PCMFM','Bitte bestätigen Sie:');

// added in RC3
define('_MD_AM_ORDER','Position');
define('_MD_AM_ORDER0','(0 = versteckt)');
define('_MD_AM_ACTIVE','Aktiv');
define('_MD_AM_INACTIVE','Inaktiv');
define('_MD_AM_NOTINSTALLED','Nicht installiert');
define('_MD_AM_NOCHANGE','Keine Änderung');
define('_MD_AM_INSTALL','Installieren');
define('_MD_AM_UNINSTALL','Deinstallieren');
define('_MD_AM_SUBMIT','Abschicken');
define('_MD_AM_CANCEL','Abbrechen');
define('_MD_AM_DBUPDATE','Datenbank erfolgreich aktualisiert!');
define('_MD_AM_BTOMADMIN','Zurück zur Moduladministration');

// %s represents module name
define('_MD_AM_FAILINS','%s kann nicht installiert werden.');
define('_MD_AM_FAILACT','%s kann nicht aktiviert werden.');
define('_MD_AM_FAILDEACT','%s kann nicht deaktiviert werden.');
define('_MD_AM_FAILUPD','%s kann nicht aktualisiert werden.');
define('_MD_AM_FAILUNINS','%s kann nicht deinstalliert werden.');
define('_MD_AM_FAILORDER','%s kann nicht umsortiert werden.');
define('_MD_AM_FAILWRITE','Kann nicht im Hauptmenü aufgeführt werden.');
define('_MD_AM_ALEXISTS','Modul %s ist bereits vorhanden.');
define('_MD_AM_ERRORSC', 'Fehler:');
define('_MD_AM_OKINS','Modul %s wurde erfolgreich installiert.');
define('_MD_AM_OKACT','Modul %s wurde erfolgreich aktiviert.');
define('_MD_AM_OKDEACT','Modul %s wurde erfolgreich deaktiviert.');
define('_MD_AM_OKUPD','Modul %s wurde erfolgreich aktualisiert.');
define('_MD_AM_OKUNINS','Modul %s wurde erfolgreich deinstalliert.');
define('_MD_AM_OKORDER','Modul %s wurde erfolgreich geändert.');

define('_MD_AM_RUSUREINS', 'Zur Modulinstallation bestätigen Sie bitte');
define('_MD_AM_RUSUREUPD', 'Um das Modul zu aktualisieren bestätigen Sie bitte');
define('_MD_AM_RUSUREUNINS', 'Soll das Modul wirklich deinstalliert werden?');
define('_MD_AM_LISTUPBLKS', 'Der folgende Block wird aktualisiert. Wählen Sie den Block zum Ändern aus (Templates und Optionen).');
define('_MD_AM_NEWBLKS', 'Neue Blöcke');
define('_MD_AM_DEPREBLKS', 'Abgelehnte Blöcke');
?>
