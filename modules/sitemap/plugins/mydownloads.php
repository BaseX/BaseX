<?php
// $Id: mydownloads.php 8145 2011-11-06 21:02:10Z beckmi $
// FILE		::	weblinks.php
// AUTHOR	::	Ryuji AMANO <info@ryus.biz>
// WEB		::	Ryu's Planning <http://ryus.biz/>
//

function b_sitemap_mydownloads(){
	$xoopsDB =& Database::getInstance();

    $block = sitemap_get_categoires_map($xoopsDB->prefix("mydownloads_cat"), "cid", "pid", "title", "viewcat.php?cid=", "title");

	return $block;
}


?>