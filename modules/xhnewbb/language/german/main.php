<?php
// $Id: main.php,v 1.7 2003/03/21 13:12:24 w4z004 Exp $
//%%%%%%		Module Name phpBB  		%%%%%

define("_MD_XHNEWBB_UPDATE","Aktualisierung");
define("_MD_XHNEWBB_UPDATED","Aktualisiert");


//functions.php
define('_MD_XHNEWBB_ERROR','FEHLER');
define('_MD_XHNEWBB_NOPOSTS','Keine Beiträge');
define('_MD_XHNEWBB_GO','Los!');

//index.php
define('_MD_XHNEWBB_FORUM','Forum');
define('_MD_XHNEWBB_WELCOME','Willkommen auf dem %s Forum.');
define('_MD_XHNEWBB_TOPICS','Themen');
define('_MD_XHNEWBB_POSTS','Beiträge');
define('_MD_XHNEWBB_LASTPOST','Letzter Beitrag');
define('_MD_XHNEWBB_MODERATOR','Moderator');
define('_MD_XHNEWBB_NEWPOSTS','Neue Beiträge');
define('_MD_XHNEWBB_NONEWPOSTS','Keine neuen Beiträge');
define('_MD_XHNEWBB_PRIVATEFORUM','Privates Forum');
define('_MD_XHNEWBB_BY','Von'); // Posted by
define('_MD_XHNEWBB_TOSTART','Um Beiträge zu lesen, wählen Sie einen Forenbereich aus, den Sie besuchen möchten.');
define('_MD_XHNEWBB_TOTALTOPICSC','Themen insgesamt: ');
define('_MD_XHNEWBB_TOTALPOSTSC','Beiträge insgesamt: ');
define('_MD_XHNEWBB_TIMENOW','Es ist nun %s Uhr');
define('_MD_XHNEWBB_LASTVISIT','Ihr letzter Besuch: %s');
define('_MD_XHNEWBB_ADVSEARCH','Erweiterte Suche');
define('_MD_XHNEWBB_POSTEDON','Veröffentlicht am: ');
define('_MD_XHNEWBB_SUBJECT','Thema: ');

//page_header.php
define('_MD_XHNEWBB_MODERATEDBY','Moderiert von');
define('_MD_XHNEWBB_SEARCH','Suche');
define('_MD_XHNEWBB_SEARCHRESULTS','Suchergebnisse');
define('_MD_XHNEWBB_FORUMINDEX','Forenübersicht');
define("_MD_XHNEWBB_ALLTOPICSINDEX","Topic Index");
define('_MD_XHNEWBB_POSTNEW','Neuen Beitrag veröffentlichen');
define('_MD_XHNEWBB_REGTOPOST','Sie müssen sich erst registrieren, bevor Sie Beiträge veröffentlichen können');

//search.php
define('_MD_XHNEWBB_KEYWORDS','Suchbegriffe:');
define('_MD_XHNEWBB_SEARCHANY','Suche nach einem der Begriffe (Standard)');
define('_MD_XHNEWBB_SEARCHALL','Suche nach allen Begriffen');
define('_MD_XHNEWBB_SEARCHALLFORUMS','Suche in allen Foren');
define('_MD_XHNEWBB_FORUMC','Forum:');
define('_MD_XHNEWBB_AUTHORC','Autor:');
define('_MD_XHNEWBB_SORTBY','Sortiert nach:');
define("_MD_XHNEWBB_FORUMNAME","Name des Forums");
define('_MD_XHNEWBB_DATE','Datum');
define('_MD_XHNEWBB_TOPIC','Thema');
define('_MD_XHNEWBB_USERNAME','Username');
define('_MD_XHNEWBB_SEARCHIN','Suche in:');
define('_MD_XHNEWBB_BODY','Beitrag');
define('_MD_XHNEWBB_NOMATCH','Ihre Suchanfrage ergab keinen Treffer, versuchen Sie einen anderen Suchbegriff.');
define('_MD_XHNEWBB_POSTTIME','Uhrzeit der Veröffentlichung');

//viewforum.php
define('_MD_XHNEWBB_REPLIES','Antworten');
define('_MD_XHNEWBB_POSTER','Autor');
define('_MD_XHNEWBB_VIEWS','Gelesen');
define('_MD_XHNEWBB_MORETHAN','Neue Beiträge [ Populär ]');
define('_MD_XHNEWBB_MORETHAN2','Keine neuen Beiträge [ Populär ]');
define('_MD_XHNEWBB_TOPICSTICKY','Thema ist markiert');
define('_MD_XHNEWBB_TOPICLOCKED','Thema ist abgeschlossen (Keine Beiträge zum Thema mehr möglich)');
define('_MD_XHNEWBB_LEGEND','Legende');
define('_MD_XHNEWBB_NEXTPAGE','Nächste Seite');
define('_MD_XHNEWBB_SORTEDBY','Sortiert nach');
define('_MD_XHNEWBB_TOPICTITLE','Thementitel');
define('_MD_XHNEWBB_NUMBERREPLIES','Anzahl der Antworten');
define('_MD_XHNEWBB_TOPICPOSTER','Themen-Autor');
define('_MD_XHNEWBB_LASTPOSTTIME','Letzter Beitrag um');
define('_MD_XHNEWBB_ASCENDING','Aufsteigende Reihenfolge');
define('_MD_XHNEWBB_DESCENDING','Absteigende Reihenfolge');
define('_MD_XHNEWBB_FROMLASTDAYS','In den letzten %s Tagen');
define('_MD_XHNEWBB_THELASTYEAR','Im letzten Jahr');
define('_MD_XHNEWBB_BEGINNING','Seit Eröffnung des Forums');
define("_MD_XHNEWBB_WHRSOLVED","Status:");
define("_MD_XHNEWBB_SOLVEDNO","Nicht erübrigt");
define("_MD_XHNEWBB_SOLVEDYES","Erübrigt");
define("_MD_XHNEWBB_SOLVEDBOTH","Beide");

//viewtopic.php
define('_MD_XHNEWBB_AUTHOR','Autor');
define('_MD_XHNEWBB_LOCKTOPIC','Thema schließen');
define('_MD_XHNEWBB_UNLOCKTOPIC','Thema öffnen');
define('_MD_XHNEWBB_UNSTICKYTOPIC','Themenmarkierung aufheben');
define('_MD_XHNEWBB_STICKYTOPIC','Thema markieren');
define('_MD_XHNEWBB_MOVETOPIC','Verschiebe das Thema');
define('_MD_XHNEWBB_DELETETOPIC','Lösche das Thema');
define('_MD_XHNEWBB_TOP','Oben');
define('_MD_XHNEWBB_PARENT','Übergeordnet');
define('_MD_XHNEWBB_PREVTOPIC','Vorheriges Thema');
define('_MD_XHNEWBB_NEXTTOPIC','Nächstes Thema');

//forumform.inc
define('_MD_XHNEWBB_ABOUTPOST','Schreibrechte');
define('_MD_XHNEWBB_ANONCANPOST','<b>Gäste</b> können neue Themen und Antworten veröffentlichen');
define("_MD_XHNEWBB_PRIVATE","Dies ist ein <b>privates</b> Forum.<br />Nur User mit besonderen Rechten können in diesem Forum Themen und Antworten veröffentlichen");
define('_MD_XHNEWBB_REGCANPOST','Alle <b>User</b> können neue Themen und Antworten schreiben');
define('_MD_XHNEWBB_MODSCANPOST','Nur <b>Moderatoren und Administratoren</b> können neue Themen und Antworten schreiben');
define('_MD_XHNEWBB_PREVPAGE','Vorherige Seite');
define('_MD_XHNEWBB_QUOTE','Zitat');

// ERROR messages
define('_MD_XHNEWBB_ERRORFORUM','FEHLER: Forum nicht ausgewählt!');
define('_MD_XHNEWBB_ERRORPOST','FEHLER: Beitrag nicht ausgewählt!');
define('_MD_XHNEWBB_NORIGHTTOPOST','Sie besitzen nicht die nötigen Rechte um Beiträge in diesem Forum zu veröffentlichen.');
define('_MD_XHNEWBB_NORIGHTTOACCESS','Sie besitzen nicht die nötigen Rechte um dieses Forum zu betreten.');
define('_MD_XHNEWBB_ERRORTOPIC','FEHLER: Kein Thema ausgewählt!');
define('_MD_XHNEWBB_ERRORCONNECT','FEHLER: Kann nicht mit der Datenbank verbinden.');
define('_MD_XHNEWBB_ERROREXIST','FEHLER: Das Forum, dass Sie ausgewählt haben existiert nicht. Bitte gehen Sie zurück und versuchen Sie es erneut.');
define('_MD_XHNEWBB_ERROROCCURED','Unerwarteter Fehler');
define('_MD_XHNEWBB_COULDNOTQUERY','Kann die Datenbank nicht abfragen.');
define('_MD_XHNEWBB_FORUMNOEXIST','FEHLER - Das Forum/Thema, dass Sie gewählt haben existiert nicht. Bitte gehen Sie zurück und versuchen Sie es erneut.');
define('_MD_XHNEWBB_USERNOEXIST','Der User existiert nicht. Bitte gehen Sie zurück und versuchen Sie es erneut.');
define('_MD_XHNEWBB_COULDNOTREMOVE','FEHLER - Kann den Beitrag nicht aus der Datenbank löschen!');
define('_MD_XHNEWBB_COULDNOTREMOVETXT','FEHLER - Kann den Beitragstext nicht löschen!');

//reply.php
define('_MD_XHNEWBB_ON','Am'); //Posted on
define('_MD_XHNEWBB_USERWROTE','%s schrieb:'); // %s is username

//post.php
define("_MD_XHNEWBB_FORMTITLEINPREVIEW","Eintrag in der Vorschau");
define('_MD_XHNEWBB_EDITNOTALLOWED','Es ist Ihnen nicht gestattet, den Beitrag zu bearbeiten!');
define('_MD_XHNEWBB_EDITEDBY','Bearbeitet von ');
define('_MD_XHNEWBB_ANONNOTALLOWED','Gästen ist es nicht gestattet, Beiträge zu veröffentlichen.<br />Bitte registrieren Sie sich.');
define('_MD_XHNEWBB_THANKSSUBMIT','Danke für Ihren Beitrag!');
define('_MD_XHNEWBB_REPLYPOSTED','Eine Antwort auf Ihren Beitrag wurde verfasst.');
define('_MD_XHNEWBB_HELLO','Hallo %s,');
define('_MD_XHNEWBB_URRECEIVING','Sie haben diese E-Mail erhalten, weil eine Antwort auf dem %s Forum zu Ihrem Beitrag verfasst wurde.'); // %s is your site name
define('_MD_XHNEWBB_CLICKBELOW','Sie finden diesen Beitrag unter folgender URL:');
define("_MD_XHNEWBB_FMT_GUESTSPOSTHEADER","Gepostet von %s als Gast\n---\n\n");

//forumform.inc
define('_MD_XHNEWBB_YOURNAME','Ihr Name:');
define('_MD_XHNEWBB_LOGOUT','Abmelden');
define('_MD_XHNEWBB_REGISTER','Registrieren');
define('_MD_XHNEWBB_SUBJECTC','Titel:');
define("_MD_XHNEWBB_EDITMODEC","Modus:");
define("_MD_XHNEWBB_ALERTEDIT","<div style='background:#FE0000;color:#FFFFFF;font-size:120%;font-weight:bold;padding:3px;'>Sie bearbeiten jetzt Ihr Thema</div>");
define("_MD_XHNEWBB_GUESTNAMEC","Ihr Name:");
define("_MD_XHNEWBB_UNAMEC","User:");
define("_MD_XHNEWBB_FMT_UNAME","%s");
define('_MD_XHNEWBB_MESSAGEICON','Beitragssymbol:');
define("_MD_XHNEWBB_SOLVEDCHECKBOX","Erübrigt");
define('_MD_XHNEWBB_MESSAGEC','Beitrag:');
define('_MD_XHNEWBB_ALLOWEDHTML','Erlaubte HTML-Tags:');
define('_MD_XHNEWBB_OPTIONS','Optionen:');
define('_MD_XHNEWBB_POSTANONLY','Anonym veröffentlichen');
define('_MD_XHNEWBB_DISABLESMILEY','Smilies deaktivieren');
define('_MD_XHNEWBB_DISABLEHTML','HTML deaktivieren');
define("_MD_XHNEWBB_NEWPOSTNOTIFY", "Benachrichtigen bei neuen Beiträgen zu dieser Diskussion");
define('_MD_XHNEWBB_ATTACHSIG','Signatur anhängen');
define('_MD_XHNEWBB_POST','Veröffentlichen');
define('_MD_XHNEWBB_SUBMIT','Abschicken');
define('_MD_XHNEWBB_CANCELPOST','Veröffentlichung abbrechen');

// forumuserpost.php
define('_MD_XHNEWBB_ADD','Hinzufügen');
define('_MD_XHNEWBB_REPLY','Antworten');

// topicmanager.php
define('_MD_XHNEWBB_YANTMOTFTYCPTF','Sie haben keinen Moderatoren-Status.');
define('_MD_XHNEWBB_TTHBRFTD','Dieses Thema wurde aus der Datenbank gelöscht.');
define('_MD_XHNEWBB_RETURNTOTHEFORUM','Zurück zum Forum');
define('_MD_XHNEWBB_RTTFI','Zurück zur Forenübersicht');
define('_MD_XHNEWBB_EPGBATA','Fehler - Bitte gehen Sie zurück und versuchen es erneut.');
define('_MD_XHNEWBB_TTHBM','Das Thema wurde verschoben.');
define('_MD_XHNEWBB_VTUT','Zeige das aktualisierte Thema');
define('_MD_XHNEWBB_TTHBL','Das Thema wurde abgeschlossen.');
define('_MD_XHNEWBB_TTHBS','Das Thema wurde markiert.');
define('_MD_XHNEWBB_TTHBUS','Die Themenmarkierung wurde aufgehoben.');
define('_MD_XHNEWBB_VIEWTHETOPIC','Thema anzeigen');
define('_MD_XHNEWBB_TTHBU','Das Thema wurde geöffnet.');
define('_MD_XHNEWBB_OYPTDBATBOTFTTY','Wenn Sie auf den >Lösche< Button klicken, wird das Thema mit allen damit verbundenen Beiträgen gelöscht');
define('_MD_XHNEWBB_OYPTMBATBOTFTTY','Wenn Sie auf den >Verschieben< Button klicken, wird das Thema mit allen damit verbundenen Beiträgen verschoben');
define('_MD_XHNEWBB_OYPTLBATBOTFTTY','Wenn Sie auf den >Geschlossen< Button klicken, wird das Thema mit allen damit verbundenen Beiträgen geschlossen. Sie können das Thema zu einem späteren Zeitpunkt wieder öffnen');
define('_MD_XHNEWBB_OYPTUBATBOTFTTY','Wenn Sie auf den >Öffnen< Button klicken, wird das Thema wieder zum Schreiben freigegeben. Sie können das Thema zu einem späteren Zeitpunkt wieder schließen');
define('_MD_XHNEWBB_OYPTSBATBOTFTTY','Wenn Sie auf den >Markieren< Button klicken, haben Sie dieses Thema markiert. Sie können die Markierung zu einem späteren Zeitpunkt wieder aufheben.');
define('_MD_XHNEWBB_OYPTTBATBOTFTTY','Wenn Sie auf den >Markierung aufheben< Button klicken, haben Sie die Markierung für dieses Thema wieder aufgehoben. Sie können es zu einem späteren Zeitpunkt wieder markieren.');
define('_MD_XHNEWBB_MOVETOPICTO','Verschiebe Thema nach:');
define('_MD_XHNEWBB_NOFORUMINDB','Es liegt kein Forum in der Datenbank vor');
define('_MD_XHNEWBB_DATABASEERROR','Datenbankfehler');
define('_MD_XHNEWBB_DELTOPIC','Thema löschen');

// delete.php
define('_MD_XHNEWBB_DELNOTALLOWED','Verzeihung, aber Sie haben nicht die nötigen Rechte, um diesen Beitrag zu löschen.');
define('_MD_XHNEWBB_AREUSUREDEL','Sind Sie sicher, dass Sie diesen Beitrag und alle damit verbundenen Antworten löschen wollen?');
define('_MD_XHNEWBB_POSTSDELETED','Ausgewählter Beitrag und die damit verbundenen Antworten wurden erfolgreich gelöscht.');

// definitions moved from global.
define('_MD_XHNEWBB_THREAD','Diskussion');
define('_MD_XHNEWBB_FROM','Aus:');
define('_MD_XHNEWBB_JOINED','Registriert seit:');
define('_MD_XHNEWBB_ONLINE','Online');
define('_MD_XHNEWBB_BOTTOM','Unten');
?>
