<?php
// $Id: blocks.php,v 1.7 2003/03/21 13:12:23 w4z004 Exp $
// Blocks
define('_MB_XHNEWBB_FORUM','Forum');
define('_MB_XHNEWBB_TOPIC','Thema');
define('_MB_XHNEWBB_RPLS','Antworten');
define('_MB_XHNEWBB_VIEWS','Gelesen');
define('_MB_XHNEWBB_LPOST','Letzter Beitrag');
define('_MB_XHNEWBB_VSTFRMS','Forenindex');
define("_MB_XHNEWBB_LISTALLTOPICS","Themenindex");
define('_MB_XHNEWBB_DISPLAY','Zeige %s Beiträge');
define('_MB_XHNEWBB_DISPLAYF','Forum in voller Größe anzeigen');
define("_MB_XHNEWBB_MARKISUP","Markierte Themen zuerst");

define("_MB_XHNEWBB_ORDERTIMED","Den neuesten zuerst");
define("_MB_XHNEWBB_ORDERVIEWSD","Den am häufigsten angesehenen zuerst");
define("_MB_XHNEWBB_ORDERREPLIESD","Den am häufigsten kommentierten zuerst");

define("_MB_XHNEWBB_CLASSPUBLIC","Nur öffentliche Foren");
define("_MB_XHNEWBB_CLASSBOTH","Alle Forentypen");
define("_MB_XHNEWBB_CLASSPRIVATE","Nur private Foren");
?>
