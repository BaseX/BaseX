Hallo {X_UNAME},

ein neuer Beitrag wurde zum Thema "{THREAD_NAME}" veröffentlicht.

Sie finden diesen Beitrag unter folgender URL:
{POST_URL}

-----------

Sie erhalten diese Nachricht, weil Sie benachrichtigt werden wollten, wenn neue Beiträge zu diesem Thema veröffentlicht werden.

Wenn dies ein Fehler ist, und Sie keine neuen Nachrichten dieser Art mehr empfangen wollen, folgen Sie diesem Link:
{X_UNSUBSCRIBE_URL}

Bitte nicht auf diese Nachricht antworten.

-----------
{X_SITENAME} ({X_SITEURL}) 
Der Webmaster
{X_ADMINMAIL}
