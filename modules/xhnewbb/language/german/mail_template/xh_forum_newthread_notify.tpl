Hallo {X_UNAME},

ein neues Thema "{THREAD_NAME}" wurde im Forum "{FORUM_NAME}" eröffnet.

Folgen Sie dem Link um das Thema zu lesen:
{THREAD_URL}

Folgen Sie diesem Link um zum Forum zu kommen:
{FORUM_URL}

-----------

Sie haben derzeit dieses Thema abonniert.

Wenn dies ein Fehler ist, und Sie keine neuen Nachrichten dieser Art mehr empfangen wollen, folgen Sie diesem Link:
{X_UNSUBSCRIBE_URL}

Bitte nicht auf diese Nachricht antworten.

-----------
{X_SITENAME} ({X_SITEURL}) 
Der Webmaster
{X_ADMINMAIL}
