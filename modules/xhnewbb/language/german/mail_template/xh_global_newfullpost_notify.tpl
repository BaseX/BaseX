Hallo {X_UNAME},

ein neuer Beitrag wurde im Modul {X_MODULE} zur Diskussion "{THREAD_NAME}" im Forum "{FORUM_NAME}" veröffentlicht.

Sie finden diesen Beitrag im Forum unter folgender URL (dort können Sie den Beitrag beantworten):
{POST_URL}

-----------
{POST_NAME}

{POST_CONTENT}
-----------

Sie erhalten diese Nachricht, weil Sie bei allen neuen Beiträgen benachrichtigt werden wollen.

Wenn dies ein Fehler ist und Sie keine weiteren Nachrichten dieser Art mehr empfangen wollen, folgen Sie diesem Link:
{X_UNSUBSCRIBE_URL}

Bitte nicht auf diese Nachricht antworten.

-----------
{X_SITENAME} ({X_SITEURL}) 
Der Webmaster
{X_ADMINMAIL}
