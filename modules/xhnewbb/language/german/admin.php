<?php
// $Id: admin.php,v 1.7 2003/03/21 13:12:23 w4z004 Exp $
//%%%%%%	File Name  index.php   	%%%%%
define('_MD_XHNEWBB_A_FORUMCONF','Forenkonfiguration');
define('_MD_XHNEWBB_A_ADDAFORUM','Forum hinzufügen');
define('_MD_XHNEWBB_A_LINK2ADDFORUM','Dieser Link bringt Sie zu der Seite, in der Sie ein Forum hinzufügen können.');
define('_MD_XHNEWBB_A_EDITAFORUM','Forum bearbeiten');
define('_MD_XHNEWBB_A_LINK2EDITFORUM','Dieser Link erlaubt es Ihnen, ein Forum zu bearbeiten.');
define('_MD_XHNEWBB_A_SETPRIVFORUM','Berechtigungen für privates Forum setzen');
define('_MD_XHNEWBB_A_LINK2SETPRIV','Dieser Link erlaubt es Ihnen, ein Forum zu einem privaten Bereich zu machen.');
define('_MD_XHNEWBB_A_SYNCFORUM','Forum/Themen-Index synchronisieren');
define('_MD_XHNEWBB_A_LINK2SYNC','Dieser Link erlaubt es Ihnen, Ihre Foren- und Themenbereiche zu synchronisieren');
define('_MD_XHNEWBB_A_ADDACAT','Kategorie hinzufügen');
define('_MD_XHNEWBB_A_LINK2ADDCAT','Mit diesem Link können Sie eine neue Kategorie hinzufügen.');
define('_MD_XHNEWBB_A_EDITCATTTL','Kategorietitel bearbeiten');
define('_MD_XHNEWBB_A_LINK2EDITCAT','Hier können Sie den Titel einer Kategorie ändern.');
define('_MD_XHNEWBB_A_RMVACAT','Kategorie löschen');
define('_MD_XHNEWBB_A_LINK2RMVCAT','Mit diesem Link können Sie eine Kategorie löschen');
define('_MD_XHNEWBB_A_REORDERCAT','Kategorien neu ordnen');
define('_MD_XHNEWBB_A_LINK2ORDERCAT','Hier können Sie die Reihenfolge der Kategorien ändern');

//%%%%%%        File Name  admin_forums.php           %%%%%
define('_MD_XHNEWBB_A_FORUMUPDATED','Forum aktualisiert');
define('_MD_XHNEWBB_A_HTSMHNBRBITHBTWNLBAMOTF','Der ausgewählte Moderator konnte nicht gelöscht werden, da dieses Forum sonst ohne Moderator wäre.');
define('_MD_XHNEWBB_A_FORUMREMOVED','Forum gelöscht.');
define('_MD_XHNEWBB_A_FRFDAWAIP','Forum mit allen Beiträgen aus der Datenbank löschen.');
define('_MD_XHNEWBB_A_NOSUCHFORUM','Kein solches Forum');
define('_MD_XHNEWBB_A_EDITTHISFORUM','Diese Forum bearbeiten');
define('_MD_XHNEWBB_A_DTFTWARAPITF','Forum löschen (Es werden dabei alle Beiträge in dem Forum gelöscht!)');
define('_MD_XHNEWBB_A_FORUMNAME','Name des Forums:');
define('_MD_XHNEWBB_A_FORUMDESCRIPTION','Beschreibung des Forums:');
define('_MD_XHNEWBB_A_MODERATOR','Moderator(en):');
define('_MD_XHNEWBB_A_REMOVE','Löschen');
define('_MD_XHNEWBB_A_NOMODERATORASSIGNED','Kein Moderator zugewiesen');
define('_MD_XHNEWBB_A_NONE','Kein');
define('_MD_XHNEWBB_A_CATEGORY','Kategorie:');
define('_MD_XHNEWBB_A_ANONYMOUSPOST','Gäste dürfen Beiträge schreiben');
define('_MD_XHNEWBB_A_REGISTERUSERONLY','Nur registrierte User');
define('_MD_XHNEWBB_A_MODERATORANDADMINONLY','Nur Moderatoren/Administratoren');
define('_MD_XHNEWBB_A_TYPE','Typ:');
define('_MD_XHNEWBB_A_PUBLIC','Öffentlich');
define('_MD_XHNEWBB_A_PRIVATE','Privat');
define('_MD_XHNEWBB_A_SELECTFORUMEDIT','Wählen Sie ein Forum aus, welches Sie bearbeiten möchten.');
define('_MD_XHNEWBB_A_NOFORUMINDATABASE','Keine Foren in der Datenbank');
define('_MD_XHNEWBB_A_DATABASEERROR','Datenbankfehler');
define('_MD_XHNEWBB_A_EDIT','Bearbeiten');
define('_MD_XHNEWBB_A_CATEGORYUPDATED','Kategorie aktualisiert.');
define('_MD_XHNEWBB_A_EDITCATEGORY','Kategorie bearbeiten:');
define('_MD_XHNEWBB_A_CATEGORYTITLE','Kategorietitel:');
define('_MD_XHNEWBB_A_SELECTACATEGORYEDIT','Wählen Sie die Kategorie aus, welche Sie bearbeiten möchten.');
define('_MD_XHNEWBB_A_CATEGORYCREATED','Kategorie erstellt.');
define('_MD_XHNEWBB_A_NTWNRTFUTCYMDTVTEFS','Hinweis: Das löscht nicht die Foren aus der Kategorie, Sie müssen dies über die "Forum bearbeiten"-Funktion ändern.');
define('_MD_XHNEWBB_A_REMOVECATEGORY','Kategorie löschen');
define('_MD_XHNEWBB_A_CREATENEWCATEGORY','Neue Kategorie erstellen');
define('_MD_XHNEWBB_A_YDNFOATPOTFDYAA','Sie haben das Formular nicht komplett ausgefüllt.<br />Haben Sie wenigstens einen Moderator zugewiesen? Bitte gehen Sie zurück und korrigieren Sie den Fehler.');
define('_MD_XHNEWBB_A_FORUMCREATED','Forum erstellt.');
define('_MD_XHNEWBB_A_VTFYJC','Zeige das gerade erstellte Forum an.');
define('_MD_XHNEWBB_A_EYMAACBYAF','Fehler! Sie müssen erst eine Kategorie hinzufügen um darin ein Forum zu erstellen');
define('_MD_XHNEWBB_A_CREATENEWFORUM','Neues Forum erstellen');
define('_MD_XHNEWBB_A_ACCESSLEVEL','Zugriffs-Level:');
define('_MD_XHNEWBB_A_CATEGORYMOVEUP','Kategorie nach oben verschieben');
define('_MD_XHNEWBB_A_TCIATHU','Kategorie ist schon ganz oben.');
define('_MD_XHNEWBB_A_CATEGORYMOVEDOWN','Kategorie nach unten verschieben');
define('_MD_XHNEWBB_A_TCIATLD','Kategorie ist schon ganz unten.');
define('_MD_XHNEWBB_A_SETCATEGORYORDER','Reihenfolge der Kategorien festlegen');
define('_MD_XHNEWBB_A_TODHITOTCWDOTIP','So wie hier werden die Kategorien auch im Foren-Index dargestellt.');
define('_MD_XHNEWBB_A_ECWMTCPUODITO','Jeder Klick verschiebt die Kategorie ein Feld nach oben oder unten.');
define('_MD_XHNEWBB_A_CATEGORY1','Kategorie');
define('_MD_XHNEWBB_A_MOVEUP','Nach oben verschieben');
define('_MD_XHNEWBB_A_MOVEDOWN','Nach unten verschieben');


define('_MD_XHNEWBB_A_FORUMUPDATE','Forumeinstellungen aktualisiert!');
define('_MD_XHNEWBB_A_RETURNTOADMINPANEL','Zurück zum Administrations-Index.');
define('_MD_XHNEWBB_A_RETURNTOFORUMINDEX','Zurück zum Forums-Index');
define('_MD_XHNEWBB_A_ALLOWHTML','HTML in den Foren erlauben:');
define('_MD_XHNEWBB_A_YES','Ja');
define('_MD_XHNEWBB_A_NO','Nein');
define('_MD_XHNEWBB_A_ALLOWSIGNATURES','Signaturen in den Foren anzeigen:');
define('_MD_XHNEWBB_A_HOTTOPICTHRESHOLD','Beiträge bevor ein Thema "heiss" wird:');
define('_MD_XHNEWBB_A_POSTPERPAGE','Beiträge pro Seite:');
define('_MD_XHNEWBB_A_TITNOPPTTWBDPPOT','(dies ist die Anzahl der Antworten zu einem Thema bevor eine 2. Seite angelegt wird)');
define('_MD_XHNEWBB_A_TOPICPERFORUM','Themen pro Forum:');
define('_MD_XHNEWBB_A_TITNOTPFTWBDPPOAF','(dies ist die Anzahl der Themen die in einer Forenseite angezeigt werden)');
define('_MD_XHNEWBB_A_SAVECHANGES','Änderungen sichern');
define('_MD_XHNEWBB_A_CLEAR','Zurücksetzen');
define('_MD_XHNEWBB_A_CLICKBELOWSYNC','Dies synchronisiert die Foren und Themen mit Ihrer Datenbank.');
define('_MD_XHNEWBB_A_SYNCHING','Synchronisiere Foren- und Themen-Index (Kann eine Zeit lang dauern)');
define('_MD_XHNEWBB_A_DONESYNC','Fertig!');
define('_MD_XHNEWBB_A_CATEGORYDELETED','Kategorie wurde gelöscht.');

//%%%%%%        File Name  admin_priv_forums.php           %%%%%

define('_MD_XHNEWBB_A_SAFTE','Wählen Sie ein Forum zum Bearbeiten');
define('_MD_XHNEWBB_A_NFID','Kein Forum in der Datenbank');
define('_MD_XHNEWBB_A_EFPF','Bearbeite Forumberechtigung für: <b>%s</b>');
define('_MD_XHNEWBB_A_UWA','Besucher mit Zugriff:');
define('_MD_XHNEWBB_A_UWOA','Besucher ohne Zugriff:');
define('_MD_XHNEWBB_A_ADDUSERS','User hinzufügen -->');
define('_MD_XHNEWBB_A_CLEARALLUSERS','Lösche alle User');
define('_MD_XHNEWBB_A_REVOKEPOSTING','Widerrufe schreiben');
define('_MD_XHNEWBB_A_GRANTPOSTING','Erlaube Schreiben');
?>
