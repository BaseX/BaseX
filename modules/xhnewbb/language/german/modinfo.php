<?php
// $Id: modinfo.php,v 1.7 2003/03/21 13:12:24 w4z004 Exp $
// Module Info

// The name of this module
define('_MI_XHNEWBB_NAME','Forum');

// A brief description of this module
define('_MI_XHNEWBB_DESC','Forenmodul für Xoops');

// Names of blocks for this module (Not all module has blocks)
define('_MI_XHNEWBB_BNAME1','Aktuelle Themen im Forum');
define('_MI_XHNEWBB_BNAME2','Die meistgelesensten Themen');
define('_MI_XHNEWBB_BNAME3','Die aktivsten Themen');
define('_MI_XHNEWBB_BNAME4','Aktuelle private Themen');
define("_MI_XHNEWBB_BDESC1","Dieser Block kann für mehrere Zwecke verwendet werden. Natürlich können Sie ihn auch mehrfach einsetzen.");

// Names of admin menu items
define('_MI_XHNEWBB_ADMENU1','Forum hinzufügen');
define('_MI_XHNEWBB_ADMENU2','Forum bearbeiten');
define('_MI_XHNEWBB_ADMENU3','Privates Forum bearbeiten');
define('_MI_XHNEWBB_ADMENU4','Foren/Themen synchronisieren');
define('_MI_XHNEWBB_ADMENU5','Kategorie hinzufügen');
define('_MI_XHNEWBB_ADMENU6','Kategorie bearbeiten');
define('_MI_XHNEWBB_ADMENU7','Kategorie löschen');
define('_MI_XHNEWBB_ADMENU8','Kategorien neu ordnen');
define("_MI_XHNEWBB_ADMENU_MYBLOCKSADMIN","Blöcke&Gruppen");

// Notification event descriptions and mail templates

define ('_MI_XHNEWBB_THREAD_NOTIFY', 'Diskussion');
define ('_MI_XHNEWBB_THREAD_NOTIFYDSC', 'Benachrichtigungsoptionen die die aktuelle Diskussion betreffen.');

define ('_MI_XHNEWBB_FORUM_NOTIFY', 'Forum');
define ('_MI_XHNEWBB_FORUM_NOTIFYDSC', 'Benachrichtigungsoptionen die das aktuelle Forum betreffen.');

define ('_MI_XHNEWBB_GLOBAL_NOTIFY', 'Allgemeine Einstellungen');
define ('_MI_XHNEWBB_GLOBAL_NOTIFYDSC', 'Allgemeine Forum-Benachrichtigungsoptionen.');

define ('_MI_XHNEWBB_THREAD_NEWPOST_NOTIFY', 'Neuer Beitrag');
define ('_MI_XHNEWBB_THREAD_NEWPOST_NOTIFYCAP', 'Benachrichtigen bei neuen Beiträgen in der aktuellen Diskussion.');
define ('_MI_XHNEWBB_THREAD_NEWPOST_NOTIFYDSC', 'Benachrichtigung bei neuen Beiträgen in der aktuellen Diskussion.');
define ('_MI_XHNEWBB_THREAD_NEWPOST_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} Automatische Benachrichtigung: Neuer Beitrag in Diskussion');

define ('_MI_XHNEWBB_FORUM_NEWTHREAD_NOTIFY', 'Neues Thema');
define ('_MI_XHNEWBB_FORUM_NEWTHREAD_NOTIFYCAP', 'Benachrichtigen bei neuen Themen im aktuellen Forum.');
define ('_MI_XHNEWBB_FORUM_NEWTHREAD_NOTIFYDSC', 'Benachrichtigung bei neuen Themen im aktuellen Forum.');
define ('_MI_XHNEWBB_FORUM_NEWTHREAD_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} Automatische Benachrichtigung: Neues Thema in Forum');

define ('_MI_XHNEWBB_GLOBAL_NEWFORUM_NOTIFY', 'Neues Forum');
define ('_MI_XHNEWBB_GLOBAL_NEWFORUM_NOTIFYCAP', 'Benachrichtigen wenn ein neues Forum angelegt worden ist.');
define ('_MI_XHNEWBB_GLOBAL_NEWFORUM_NOTIFYDSC', 'Benachrichtigung wenn ein neues Forum angelegt worden ist.');
define ('_MI_XHNEWBB_GLOBAL_NEWFORUM_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} Automatische Benachrichtigung: Neues Forum');

define ('_MI_XHNEWBB_GLOBAL_NEWPOST_NOTIFY', 'Neuer Beitrag');
define ('_MI_XHNEWBB_GLOBAL_NEWPOST_NOTIFYCAP', 'Benachrichtigen bei neuen Beiträgen.');
define ('_MI_XHNEWBB_GLOBAL_NEWPOST_NOTIFYDSC', 'Benachrichtigung bei neuen Beiträgen.');
define ('_MI_XHNEWBB_GLOBAL_NEWPOST_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} Automatische Benachrichtigung: Neuer Beitrag');

define ('_MI_XHNEWBB_FORUM_NEWPOST_NOTIFY', 'Neuer Beitrag');
define ('_MI_XHNEWBB_FORUM_NEWPOST_NOTIFYCAP', 'Benachrichtigen bei neuen Beiträgen im aktuellen Forum.');
define ('_MI_XHNEWBB_FORUM_NEWPOST_NOTIFYDSC', 'Benachrichtigung bei neuen Beiträgen im aktuellen Forum.');
define ('_MI_XHNEWBB_FORUM_NEWPOST_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} Automatische Benachrichtigung: Neuer Beitrag im aktuellen Forum');

define ('_MI_XHNEWBB_GLOBAL_NEWFULLPOST_NOTIFY', 'Neuer Beitrag (Kompletter Text)');
define ('_MI_XHNEWBB_GLOBAL_NEWFULLPOST_NOTIFYCAP', 'Benachrichtigen bei neuen Beiträgen (Kompletten Text in Benachrichtigung anzeigen).');
define ('_MI_XHNEWBB_GLOBAL_NEWFULLPOST_NOTIFYDSC', 'Benachrichtigung bei neuen Beiträgen (Kompletten Text in Benachrichtigung anzeigen).');
define ('_MI_XHNEWBB_GLOBAL_NEWFULLPOST_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} Automatische Benachrichtigung: Neuer Beitrag (Kompletter Text)');


?>
