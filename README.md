BaseX
=====
[![Software License](https://img.shields.io/badge/license-GPL-brightgreen.svg?style=flat)](LICENSE)
[![Translations on Transifex](http://base-x.org/images/translations-transifex-blue.svg)](https://www.transifex.com/base-x)

Building a distribution based on XOOPS 2.0.18.2, 

because I really liked it back in 2008 and don't really care for the 2.6.x direcetion is going. And 2.5.7x seams to be really large.

I am doing this distribution along with some modules and lot's of hacks to the core.


=== Features ===
----------------

These features are included in the BaseX CMS basic:

 * News Module
 * About Us method
 * Multilanguage method
 * German Language included
 * Contact Module
 * Legal information method
 * EU Cookie Compliance method
 * Smart Themes (mobile ready)
 * Mobile ready skeleton theme
 * Most common CMS 'basic needs' modules included, install as needed.


=== Planned Feature ===
-----------------------

These features are being planned and hopfully soon realized.

  * Get rid of unneeded or unused stuff
  * Skeleton Module
  * Captcha handeling in the Admin area
  * Move PM and Profile Links to USERMENU and out of MAINMENU

 
=== More Info ===
-----------------

We will have the site up during the upgrade process as follows...

 * Main Site = will be up using some of the old stuff and will be upgraded as we move allong, if this proves to become difficult we will notify of the changes early. ==> http://www.base-x.org
 * Dev Area = is here at [ GitLab ]( https://gitlab.com/hyperclock/BaseX ).
 * Dev Info Site will be ==> http://dev.base-x.org



=== Help this project ===
-------------------------

If you woul like to help with this project, then eMail the project and let us know where you'd like to help out. Zhe more help we get the faster this will get released.

eMail ==> info AT base-x DOT org
