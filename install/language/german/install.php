<?php
// german v 1.0 2007/08/27 sato-san / Rene Sato / www.XOOPS-magazine.com $
define('_INSTALL_L0','Willkommen beim Installationsassistenten für BaseX');
define('_INSTALL_L70','Bitte setzen Sie die Rechte der mainfile.php so, dass sie beschrieben werden kann ( chmod 777 mainfile.php auf einem UNIX/LINUX Server, auf einem Windows Server -read-only- ). Laden Sie die Seite neu, wenn Sie die Einstellungen geändert haben.');
define('_INSTALL_L1','Öffnen Sie die mainfile.php mit einem Texteditor und suchen Sie folgenden Code in Zeile 31');
define('_INSTALL_L2','Jetzt ändern Sie diese in:');
define('_INSTALL_L3','jetzt, in Zeile 35, ändern %s in %s');
define('_INSTALL_L4','OK, nach den geänderten Einstellungen versuchen Sie es bitte noch einmal!');
define('_INSTALL_L5','Warnung!');
define('_INSTALL_L6','Es wurde ein Fehler in Ihrer XOOPS_ROOT_PATH Konfiguration in Zeile 31 der mainfile.php gefunden.');
define('_INSTALL_L7','Ihre Einstellungen: ');
define('_INSTALL_L8','Wir haben gefunden: ');
define('_INSTALL_L9','(Auf MS-Plattformen können diese Fehler auftreten, selbst wenn Ihre Einstellungen korrekt sind, wenn das der Fall ist, bestätigen Sie mit dem unteren Button um fortzufahren.)');
define('_INSTALL_L10','Bitte klicken Sie auf den Button um fortzufahren wenn dies OK ist.');
define('_INSTALL_L11','Der Server-Pfad zu Ihrem BaseX-Root-Verzeichnis: ');
define('_INSTALL_L12','URL zu Ihrem BaseX-Root-Verzeichnis: ');
define('_INSTALL_L13','Wenn die oben genannten Einstellungen korrekt sind, klicken Sie auf den Button, um fortzufahren. <br />(Es werden noch keine Tabellen erstellt)');
define('_INSTALL_L14','Weiter');
define('_INSTALL_L15','Öffnen Sie bitte mainfile.php und geben Sie angeforderte Datenbankdaten ein ');
define('_INSTALL_L16','%s ist der Hostname Ihres Datenbank-Servers.');
define('_INSTALL_L17','%s ist der Username Ihres Datenbank-Kontos.');
define('_INSTALL_L18','%s ist das Passwort welches verlangt wird, um auf Ihre Datenbank zuzugreifen.');
define('_INSTALL_L19','%s ist der Name Ihrer Datenbank in der die BaseX-Tabellen erstellt werden.');
define('_INSTALL_L20','%s ist der Präfix für Ihre Tabellen. Das wird während der Installation gemacht.');
define('_INSTALL_L21','Es konnte keine Datenbank mit folgendem Namen auf Ihrem Server gefunden werden:');
define('_INSTALL_L22','Wollen Sie diese Datenbank erstellen?');
define('_INSTALL_L23','Ja');
define('_INSTALL_L24','Nein');
define('_INSTALL_L25','Wir haben die folgenden Datenbankinformationen aus Ihrer Konfiguration in der mainfile.php ermittelt. Verbessern Sie diese jetzt, wenn die Einstellungen nicht korrekt sind.');
define('_INSTALL_L26','Datenbankkonfiguration');
define('_INSTALL_L51','<b>Datenbank</b>');
define('_INSTALL_L66','Wählen Sie die zu verwendende Datenbank');
define('_INSTALL_L27','<b>Datenbank Hostname</b>');
define('_INSTALL_L67','Hostname der Datenbank. Wenn Sie unsicher sind, " localhost " funktioniert in den meisten Fällen.');
define('_INSTALL_L28','<b>Datenbank Username</b>');
define('_INSTALL_L65','Ihr Datenbank Userkonto');
define('_INSTALL_L29','<b>Datenbank Name</b>');
define('_INSTALL_L64','Der Name Ihrer Datenbank. Der Installationsassistent wird versuchen diese zu erstellen, falls sie noch nicht existiert.');
define('_INSTALL_L52','<b>Datenbank Passwort</b>');
define('_INSTALL_L68','Das Passwort für Ihre Datenbank');
define('_INSTALL_L30','<b>Tabellen-Präfix</b>');
define('_INSTALL_L63','Dieser Präfix wird allen neuen Tabellen vorangestellt, um Namenskonflikte in der Datenbank zu vermeiden. Wenn Sie unsicher sind behalten Sie den Präfix " xoops " bei.');
define('_INSTALL_L54','<b>Brauchen Sie eine permanente Verbindung?</b>');
define('_INSTALL_L69','Standardmässig "Nein". Wählen Sie "Nein" wenn Sie unsicher sind.');
define('_INSTALL_L55','<b>BaseX Physikalischer Pfad</b>');
define('_INSTALL_L59','Physikalischer Pfad zu Ihrem BaseX-Verzeichnis ohne abschliessenden Slash.');
define('_INSTALL_L56','<b>BaseX Virtueller Pfad (URL)</b>');
define('_INSTALL_L58','Virtueller Pfad zu Ihrem BaseX-Verzeichnis ohne abschliessenden Slash.');

define('_INSTALL_L31','Datenbank konnte nicht erstellt werden. Sprechen Sie mit ihrem Server-Administrator falls Sie Details benötigen.');
define('_INSTALL_L32','Installation abgeschlossen!');
define('_INSTALL_L33','Klicken Sie <a href="../index.php"><font color="blue">hier</font></a>, um zur Startseite Ihrer neuen Website zu gelangen.');
define('_INSTALL_L35','Wenn irgendwelche Fehler auftraten, setzen Sie sich bitte mit dem deutschen Support-Team auf <a href="http://www.base-x.org/">BaseX DevTeam</a> in Verbindung.');
define('_INSTALL_L36','Wählen Sie bitte Ihren Admin-Namen und Passwort und klicken Sie unten auf den Button, um die Erstellung der Tabellen in der Datenbank zu starten.');
define('_INSTALL_L37','Admin Name');
define('_INSTALL_L38','Admin E-Mail');
define('_INSTALL_L39','Admin Passwort');
define('_INSTALL_L74','Admin Passwort wiederholen');
define('_INSTALL_L40','Erstelle Tabellen');
define('_INSTALL_L41','Bitte gehen Sie zurück und füllen Sie alle erforderlichen Felder aus.');
define('_INSTALL_L42','zurück');
define('_INSTALL_L57','Bitte tragen Sie ein: %s');

// %s is database name
define('_INSTALL_L43','Datenbank %s wurde erstellt!');

// %s is table name
define('_INSTALL_L44','Konnte %s nicht erstellen.');
define('_INSTALL_L45','Tabelle %s erstellt.');

define("_INSTALL_L46","Damit die Module in diesem Paket einwandfrei arbeiten, m&uuml;ssen folgende Dateien beschreibbar durch den Server sein. Die Berechtigungen f&uuml;r diese Dateien m&uuml;ssen ge&auml;ndert werden. (Z. B.  'chmod 666 Dateiname' und 'chmod 777 Verzeichnisname' auf einem UNIX/LINUX-Server, oder &Uuml;berpr&uuml;fung der Dateieigenschaften der Datei/Verzeichnis - nur-lesen darf NICHT gesetzt sein - auf einem Windows-Server)");
define('_INSTALL_L47','Weiter');

define('_INSTALL_L53','Bitte bestätigen Sie die folgenden eingetragenen Daten:');

define('_INSTALL_L60','Konnte die mainfile.php nicht öffnen. Bitte überprüfen Sie die Zugriffsrechte der Datei und versuchen Sie es erneut.');
define('_INSTALL_L61','Konnte nicht in die mainfile.php schreiben. Kontaktieren Sie den Server-Administrator für Details.');
define('_INSTALL_L62','Konfigurationsdaten wurden erfolgreich gespeichert. Klicken Sie auf den Button um fortzufahren.');
define('_INSTALL_L72','Die folgenden Verzeichnise müssen mit Schreibrechten vom Server erstellt werden. (z. B. "chmod 777 Verzeichnis_name" auf einem UNIX/LINUX-Server)');
define('_INSTALL_L73','Ungültige E-Mail-Adresse');

// add by haruki
define('_INSTALL_L80','Anleitung');
define('_INSTALL_L81','Prüfe Dateiberechtigung');
define('_INSTALL_L82','Prüfe Datei- und Verzeichnisberechtigung.');
define('_INSTALL_L83','Datei %s ist nicht beschreibbar.');
define('_INSTALL_L84','Datei %s ist beschreibbar.');
define('_INSTALL_L85','Verzeichnis %s ist NICHT beschreibbar.');
define('_INSTALL_L86','Verzeichnis %s ist beschreibbar.');
define('_INSTALL_L87','Keine Fehler gefunden.');
define('_INSTALL_L89','Allgemeine Einstellungen');
define('_INSTALL_L90','Allgemeine Konfiguration');
define('_INSTALL_L91','Wiederholen');
define('_INSTALL_L92','Speichere Einstellungen');
define('_INSTALL_L93','Bearbeite Einstellungen');
define('_INSTALL_L88','Speichere Konfigurationsdaten.');
define('_INSTALL_L94','Überprüfe Pfad & URL');
define('_INSTALL_L127','Überprüfe Dateipfad- & URL-Einstelungen.');
define('_INSTALL_L95','Kann physikalischen Pfad zu Ihrem BaseX-Verzeichnis nicht finden.');
define('_INSTALL_L96','Es gibt einen Konflikt zwischen dem ermittelten physikalischen Pfad (%s) und Ihren Eingaben');
define('_INSTALL_L97','<b>Physikalischer Pfad</b> ist in Ordnung.');

define('_INSTALL_L99','<b>Physikalischer Pfad</b> muß ein Verzeichnis sein.');
define('_INSTALL_L100','<b>Virtueller Pfad</b> Ihre Eingabe ist die korrekte URL.');
define('_INSTALL_L101','<b>Virtueller Pfad</b> Ihre Eingabe ist keine korrekte URL.');
define('_INSTALL_L102','Überprüfe Datenbank-Einstellungen');
define('_INSTALL_L103','Neustart');
define('_INSTALL_L104','Überprüfe Datenbank');
define('_INSTALL_L105','Versuche Datenbank zu erstellen');
define('_INSTALL_L106','Kann nicht zum Datenbankserver verbinden.');
define('_INSTALL_L107','Überprüfen Sie bitte den Datenbankserver und seine Konfiguration.');
define('_INSTALL_L108','Verbindung zum Datenbankserver ist OK.');
define('_INSTALL_L109','Datenbank %s existiert nicht.');
define('_INSTALL_L110','Datenbank %s existiert und ist verfügbar.');
define('_INSTALL_L111','Datenbank-Verbindung ist OK.<br />Klicken Sie auf den Button um die Tabellen in der Datenbank zu erstellen.');
define('_INSTALL_L112','Administrator-Einstellungen');
define('_INSTALL_L113','Tabelle %s gelöscht.');
define('_INSTALL_L114','kann Tabellen nicht erstellen.');
define('_INSTALL_L115','Tabellen in der Datenbank erstellt.');
define('_INSTALL_L116','Füge Daten ein');
define('_INSTALL_L117','Fertig');

define('_INSTALL_L118','Fehler beim Erstellen der Tabelle %s.');
define('_INSTALL_L119','%d eingetragen in die Tabelle %s.');
define('_INSTALL_L120','Fehler beim Eintragen von %d in die Tabelle %s.');

define('_INSTALL_L121','Konstante %s gespeichert in %s.');
define('_INSTALL_L122','Fehler beim Schreiben der Konstante %s.');

define('_INSTALL_L123','Datei %s im cache/-Verzeichnis gespeichert.');
define('_INSTALL_L124','Fehler beim Speichern der Datei %s ins cache/-Verzeichnis.');

define('_INSTALL_L125','Datei %s überschrieben von %s.');
define('_INSTALL_L126','Konnte Datei %s nicht schreiben .');

define('_INSTALL_L130','Der Installationsassistent hat Tabellen für XOOPS 1.3.x in Ihrer Datenbank ermittelt.<br />Der Installationsassistent versucht jetzt, Ihre Datenbank zu BaseX upzudaten.');
define('_INSTALL_L131','Tabellen für BaseX bestehen bereits in Ihrer Datenbank .');
define('_INSTALL_L132','Aktualisiere Tabellen');
define('_INSTALL_L133','Tabellen aktualisiert.');
define('_INSTALL_L134','Aktualisierungsfehler in Tabelle %s.');
define('_INSTALL_L135','Tabellenaktualisierung fehlgeschlagen.');
define('_INSTALL_L136','Die Tabellen der Datenbank aktualisiert.');
define('_INSTALL_L137','Aktualisierung Module');
define('_INSTALL_L138','Aktualisierung Kommentare');
define('_INSTALL_L139','Aktualisierung Avatare');
define('_INSTALL_L140','Aktualisierung Smilies');
define('_INSTALL_L141','Der Installationsassistent aktualisiert jetzt jedes Modul, um mit BaseX zu arbeiten .<br />Überprüfen Sie alle Module in ihrem BaseX-Ordner, Gegebenenfalls uplopaden Sie diese.<br />Das Update kann einige Zeit in Anspruch nehmen');
define('_INSTALL_L142','Aktualisiere Module.');
define('_INSTALL_L143','Die Konfigurationsdaten werden jetzt aktualisiert.');
define('_INSTALL_L144','Aktualisierung Konfiguration');
define('_INSTALL_L145','Kommentare werden geschrieben.');
define('_INSTALL_L146','Kommentare konnten nicht geschrieben werden.');
define('_INSTALL_L147','Aktualisiere Kommentare.');
define('_INSTALL_L148','Aktualisierung komplett.');
define('_INSTALL_L149','Die Installation versucht jetzt ihre Kommentare von der Vers. 1.3.x in die Vers. 2.0 zu konvertieren, das kann einige Zeit in Anspruch nehmen.');
define('_INSTALL_L150','Die Installation versucht jetzt Ihre Smilies und Rangbilder zu BaseX zu konvertieren, das kann einige Zeit in Anspruch nehmen.');
define('_INSTALL_L151','Die Installation versucht jetzt die Mitgliedsavatare in BaseX zu konvertieren,<br /> das kann einige Zeit in Anspruch nehmen.');
define('_INSTALL_L155','Aktualisiere Smilies/Rangbilder.');
define('_INSTALL_L156','Aktualisiere Mitglieds-Avatarbilder.');
define('_INSTALL_L157','Usergruppen zurückstellen');
define('_INSTALL_L158','Gruppen in 1.3.x');
define('_INSTALL_L159','Webmaster');
define('_INSTALL_L160','Registered Users');
define('_INSTALL_L161','Anonymous Users');
define('_INSTALL_L162','Sie müssen die Standardgruppe für jede auswählen.');
define('_INSTALL_L163','Tabelle %s markiert.');
define('_INSTALL_L164','Fehler beim Löschen der Tabelle %s.');
define('_INSTALL_L165','Diese Seite ist wegen Wartungs/Update-Arbeiten geschlossen. Bitte besuchen Sie uns zu einem späterem Zeitpunkt noch einmal.');

// %s is filename
define('_INSTALL_L152','Kann %s nicht öffnen.');
define('_INSTALL_L153','Konnte %s nicht aktualisieren.');
define('_INSTALL_L154','%s aktualisiert.');

define('_INSTALL_L128', 'Sprache wählen die während der Installation benutzt werden soll');
define('_INSTALL_L200', 'Neu laden');

define('_INSTALL_CHARSET','UTF-8');
?>
