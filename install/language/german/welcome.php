<?php
// german v 1.0 2007/08/27 sato-san / Rene Sato / www.XOOPS-magazine.com $
$content .=
"<u><b>Was ist es?</b></u>
<p><b>BaseX</b> ist eine komplette, fast <i>aus dem Kasten</i> zu verwenden, 
Content Management System. Das System ist mehrsprachig, EU-Cookie-kompatibel, 
enthält allgemeine Satz von Modulen, die auf fast allen Webseiten benötigt wird.
 Wie Kontakt, News, Sitemap, FAQ und vielleicht ein paar mehr. Fest programmiert 
 in das System sind ein paar Extras wie <i>Über uns, Impressum, AGB, Datenschutzerklärung 
 und Cookie-Informationen</i> Seiten. All das kann im Backend bearbeitet werden.Viel mehr 
 ist geplant, also das Projekt im Auge behalten, shauen wie es wächst.</p>
<p>
<b>XOOPS</b> ist ein dynamisches OO (Objekt Orientiertes) Open Source Portalskript geschrieben in PHP. Unterstützt wird es mit einer Anzahl von Datenbanken (aktuell nur mySQL), XOOPS ist somit ein ideales CMS für den Aufbau von kleineren und größeren Communities
</p>
<p>
XOOPS ist freigegeben unter den Bedingungen der <a href='http://www.gnu.org/copyleft/gpl.html' target='_blank'>GNU General Public License (GPL)</a> und ist frei zu verwenden und zu ändern.
Es ist frei, so lange Änderungen, wie Sie durch die Bestimungen der GPL genannt sind, erhalten bleiben.
</p>
<u><b>Anforderungen</b></u>
<p>
<ul>
<li>HTTP-Server (<a href='http://www.apache.org/' target='_blank'>Apache</a>, IIS, Roxen, etc)</li>
<li><a href='http://www.php.net/' target='_blank'>PHP</a> 4.0.5 und höher (4.1.1 oder höher empfohlen)</li>
<li><a href='http://www.mysql.com/' target='_blank'>MySQL</a>-Datenbank 3.23.XX</li>
</ul>
</p>
<u><b>Vorbereitungen</b></u>
<ul>
<li>Setup des HTTP-Servers, PHP und der Datenbankrechte.</li>
<li>Erstellen Sie eine Datenbank für Ihre XOOPS-Seite.</li>
<li>Bereiten Sie ein Userkonto vor und vergeben Sie dem User die Rechte zur Datenbank.</li>
<li>Die Verzeichnisse uploads/, cache/ und templates_c/ sowie die Datei mainfile.php müssen beschreibbar sein.</li>
<li>Schalten Sie Cookies und JavaScript in Ihrem Browser ein.</li>
</ul>
<u><b>Installation</b></u>
<p>
Folgen Sie dem Installationsassistenten.
</p>
"
?>
