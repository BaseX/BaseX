<?php
// $Id: welcome.php 1240 2008-01-10 04:06:08Z julionc $
$content .=
"<u><b>What is it?</b></u>
<p>
<b>BaseX</b> is a complete ready to use almost <i>out of the box</i> Content 
Management System. The system is multlingual, EU Cookie compliant, contains 
general set of modules that is needed on almost ALL websites. Such as Contact, News,
Sitemap, FAQ and maybe a few more.

Hardcoded into the system is some extras like <i>About Us, Imprint, Terms, Privacy Policy 
and Cookie Information</i> pages. All of the can be edited in the backend.

Much more is planned, so keep an eye on the project as it grows.
</p>
<p>
<b>XOOPS</b> is a dynamic OO (Object Oriented) based open source portal script written in PHP.
XOOPS supports a number of databases,
making XOOPS an ideal tool for developing small to large dynamic community websites,
intra company portals, corporate portals, weblogs and much more.
</p>
<p>
XOOPS is released under the terms of the <a href='http://www.gnu.org/copyleft/gpl.html' target='_blank'>GNU General Public License (GPL)</a> and is free to use and modify.
It is free to redistribute as long as you abide by the distribution terms of the GPL.
</p>
<u><b>Requirements</b></u>
<br />
<ul>
<li>Web Server: Any server supporting the required PHP version (<a href='http://www.apache.org' target='_blank' title='Apache'>Apache</a> highly recommended)</li>
<li><a href='http://www.php.net' target='_blank' title='PHP'>PHP</a>: Any PHP version >= 4.3 (PHP 4.2.x may work but is not officially supported)</li>
<li><a href='http://www.mysql.com' target='_blank' title='MySQL'>MySQL</a>: MySQL server 3.23+</li>
</ul>
<br />
<u><b>Before you install</b></u>
<ul>
<li>Setup WWW server, PHP and database server properly.</li>
<li>Prepare a database for your XOOPS site.</li>
<li>Prepare user account and grant the user the access to the database.</li>
<li>Make the directories of uploads/, cache/ and templates_c/ and the files of mainfile.php writabale.</li>
<li>Turn cookie and JavaScript of your browser on.</li>
</ul>
<u><b>Installation</b></u>
<p>
Follow this install wizard.
</p>
"
?>
